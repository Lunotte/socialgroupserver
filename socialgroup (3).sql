-- phpMyAdmin SQL Dump
-- version 4.6.4
-- https://www.phpmyadmin.net/
--
-- Client :  127.0.0.1
-- Généré le :  Ven 26 Janvier 2018 à 15:22
-- Version du serveur :  5.7.14
-- Version de PHP :  5.6.25

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `socialgroup`
--
CREATE DATABASE IF NOT EXISTS `socialgroup` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;
USE `socialgroup`;

-- --------------------------------------------------------

--
-- Structure de la table `sg_availabilities`
--

DROP TABLE IF EXISTS `sg_availabilities`;
CREATE TABLE `sg_availabilities` (
  `id` int(11) NOT NULL,
  `start` datetime NOT NULL,
  `end` datetime NOT NULL,
  `user_id` int(11) NOT NULL,
  `group_id` int(11) NOT NULL,
  `availability_type_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Contenu de la table `sg_availabilities`
--

INSERT INTO `sg_availabilities` (`id`, `start`, `end`, `user_id`, `group_id`, `availability_type_id`) VALUES
(1, '2017-12-19 00:00:00', '2017-12-25 00:00:00', 1, 1, 1),
(2, '2017-12-26 00:00:00', '2017-12-28 00:00:00', 1, 1, 2),
(3, '2017-12-23 00:00:00', '2017-12-26 00:00:00', 2, 2, 2);

-- --------------------------------------------------------

--
-- Structure de la table `sg_availability_types`
--

DROP TABLE IF EXISTS `sg_availability_types`;
CREATE TABLE `sg_availability_types` (
  `id` int(11) NOT NULL,
  `name` varchar(25) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Contenu de la table `sg_availability_types`
--

INSERT INTO `sg_availability_types` (`id`, `name`) VALUES
(1, 'Available'),
(2, 'Unavailable');

-- --------------------------------------------------------

--
-- Structure de la table `sg_comments`
--

DROP TABLE IF EXISTS `sg_comments`;
CREATE TABLE `sg_comments` (
  `id` int(11) NOT NULL,
  `message` text NOT NULL,
  `post_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Contenu de la table `sg_comments`
--

INSERT INTO `sg_comments` (`id`, `message`, `post_id`, `user_id`) VALUES
(4, 'Je vous redis, j\'ai pas mal de travaux à faire chez moi, plus maintenant !', 2, 2),
(5, 'Je change de vie!', 2, 2),
(14, 'Je change de vie Again!', 2, 2),
(15, 'Je change de vie Again!', 2, 1);

-- --------------------------------------------------------

--
-- Structure de la table `sg_events`
--

DROP TABLE IF EXISTS `sg_events`;
CREATE TABLE `sg_events` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `start_time` datetime NOT NULL,
  `end_time` datetime NOT NULL,
  `event_type_id` int(11) NOT NULL,
  `request_id` int(11) DEFAULT NULL,
  `user_id` int(11) NOT NULL,
  `group_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Contenu de la table `sg_events`
--

INSERT INTO `sg_events` (`id`, `name`, `description`, `start_time`, `end_time`, `event_type_id`, `request_id`, `user_id`, `group_id`) VALUES
(1, 'Présent pour des vacances ', 'Yo les gens, je suis en vacances et donc de retour dans le cotentin. Je propose entre autre d\'organiser une méga soirée.', '2018-05-12 00:00:00', '2018-05-18 00:00:00', 1, 1, 1, 1),
(2, 'Test event  1 ', 'Description Test event  1.', '2018-05-12 00:00:00', '2018-05-18 00:00:00', 1, 1, 1, 1);

-- --------------------------------------------------------

--
-- Structure de la table `sg_event_types`
--

DROP TABLE IF EXISTS `sg_event_types`;
CREATE TABLE `sg_event_types` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL COMMENT 'requete : publication dun event qui soumet un formulaire de presence '
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Contenu de la table `sg_event_types`
--

INSERT INTO `sg_event_types` (`id`, `name`) VALUES
(1, 'Disponible'),
(2, 'Indisponible'),
(3, 'Requête');

-- --------------------------------------------------------

--
-- Structure de la table `sg_galleries`
--

DROP TABLE IF EXISTS `sg_galleries`;
CREATE TABLE `sg_galleries` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `path` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Contenu de la table `sg_galleries`
--

INSERT INTO `sg_galleries` (`id`, `name`, `description`, `path`) VALUES
(2, 'Vacances 2017', 'Séjour à Londres', ''),
(3, 'Vacances 2018', 'Où allons nous aller cette année ?', ''),
(4, 'Vacances 2018', 'Où allons nous aller cette année ?', ''),
(5, 'Vacances 2018', 'Où allons nous aller cette année ?', ''),
(6, 'Vacances 2018', 'Où allons nous aller cette année ?', ''),
(7, 'Vacances 2018', 'Où allons nous aller cette année ?', ''),
(8, 'Vacances 2018', 'Où allons nous aller cette année ?', ''),
(9, 'Vacances 2018', 'Où allons nous aller cette année ?', ''),
(10, 'Vacances 2018', 'Où allons nous aller cette année ?', ''),
(11, 'Vacances 2018', 'Où allons nous aller cette année ?', '');

-- --------------------------------------------------------

--
-- Structure de la table `sg_groups`
--

DROP TABLE IF EXISTS `sg_groups`;
CREATE TABLE `sg_groups` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Contenu de la table `sg_groups`
--

INSERT INTO `sg_groups` (`id`, `name`) VALUES
(1, 'Groupe de test'),
(2, 'Groupe de test 2'),
(3, 'group 4');

-- --------------------------------------------------------

--
-- Structure de la table `sg_group_defaults`
--

DROP TABLE IF EXISTS `sg_group_defaults`;
CREATE TABLE `sg_group_defaults` (
  `id` int(11) NOT NULL,
  `value` int(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Contenu de la table `sg_group_defaults`
--

INSERT INTO `sg_group_defaults` (`id`, `value`) VALUES
(1, 1);

-- --------------------------------------------------------

--
-- Structure de la table `sg_posts`
--

DROP TABLE IF EXISTS `sg_posts`;
CREATE TABLE `sg_posts` (
  `id` int(11) NOT NULL,
  `message` text NOT NULL,
  `user_id` int(11) NOT NULL,
  `group_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Contenu de la table `sg_posts`
--

INSERT INTO `sg_posts` (`id`, `message`, `user_id`, `group_id`) VALUES
(2, 'Lu\' les gens, vous faîtes quoi de beau ce week-end ???', 1, 1),
(3, 'Test de post n°1 !!!', 1, 1);

-- --------------------------------------------------------

--
-- Structure de la table `sg_requests`
--

DROP TABLE IF EXISTS `sg_requests`;
CREATE TABLE `sg_requests` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `request_type_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Contenu de la table `sg_requests`
--

INSERT INTO `sg_requests` (`id`, `name`, `description`, `request_type_id`) VALUES
(1, 'cine', 'Star Wars', 2);

-- --------------------------------------------------------

--
-- Structure de la table `sg_request_types`
--

DROP TABLE IF EXISTS `sg_request_types`;
CREATE TABLE `sg_request_types` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Contenu de la table `sg_request_types`
--

INSERT INTO `sg_request_types` (`id`, `name`) VALUES
(1, 'Boire un verre'),
(2, 'Cinema');

-- --------------------------------------------------------

--
-- Structure de la table `sg_users`
--

DROP TABLE IF EXISTS `sg_users`;
CREATE TABLE `sg_users` (
  `id` int(11) NOT NULL,
  `email` varchar(50) NOT NULL,
  `firstname` varchar(50) NOT NULL,
  `lastname` varchar(50) NOT NULL,
  `username` varchar(25) NOT NULL,
  `password` varchar(255) NOT NULL,
  `avatar` varchar(255) DEFAULT NULL,
  `group_default_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Contenu de la table `sg_users`
--

INSERT INTO `sg_users` (`id`, `email`, `firstname`, `lastname`, `username`, `password`, `avatar`, `group_default_id`) VALUES
(1, 'charlybeaugrand@gmail.com', 'Charly', 'Beaugrand', 'charly', '$2a$10$bnC26zz//2cavYoSCrlHdecWF8tkGfPodlHcYwlACBBwJvcEf0p2G', '', 1),
(2, 'baptiste.tournaille@gmail.com', 'Baptiste', 'Tournaille', 'bapt', '$2a$10$bnC26zz//2cavYoSCrlHdecWF8tkGfPodlHcYwlACBBwJvcEf0p2G', '', 2),
(11, 'toto@gmail.com', 'Charly', 'Beaugrand', 'charly', '$2a$10$bnC26zz//2cavYoSCrlHdecWF8tkGfPodlHcYwlACBBwJvcEf0p2G', NULL, 1);

-- --------------------------------------------------------

--
-- Structure de la table `sg_user_groups`
--

DROP TABLE IF EXISTS `sg_user_groups`;
CREATE TABLE `sg_user_groups` (
  `user_id` int(11) NOT NULL,
  `group_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Contenu de la table `sg_user_groups`
--

INSERT INTO `sg_user_groups` (`user_id`, `group_id`) VALUES
(1, 1);

-- --------------------------------------------------------

--
-- Structure de la table `sg_user_roles`
--

DROP TABLE IF EXISTS `sg_user_roles`;
CREATE TABLE `sg_user_roles` (
  `user_id` int(11) NOT NULL,
  `role` varchar(25) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Contenu de la table `sg_user_roles`
--

INSERT INTO `sg_user_roles` (`user_id`, `role`) VALUES
(1, 'MEMBER'),
(11, 'MEMBER');

--
-- Index pour les tables exportées
--

--
-- Index pour la table `sg_availabilities`
--
ALTER TABLE `sg_availabilities`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_id` (`user_id`),
  ADD KEY `group_id` (`group_id`),
  ADD KEY `availability_type_id` (`availability_type_id`);

--
-- Index pour la table `sg_availability_types`
--
ALTER TABLE `sg_availability_types`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `sg_comments`
--
ALTER TABLE `sg_comments`
  ADD PRIMARY KEY (`id`),
  ADD KEY `post_id` (`post_id`),
  ADD KEY `user_id` (`user_id`);

--
-- Index pour la table `sg_events`
--
ALTER TABLE `sg_events`
  ADD PRIMARY KEY (`id`),
  ADD KEY `event_type_id` (`event_type_id`),
  ADD KEY `request_id` (`request_id`),
  ADD KEY `user_id` (`user_id`),
  ADD KEY `group_id` (`group_id`);

--
-- Index pour la table `sg_event_types`
--
ALTER TABLE `sg_event_types`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `sg_galleries`
--
ALTER TABLE `sg_galleries`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `sg_groups`
--
ALTER TABLE `sg_groups`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `sg_group_defaults`
--
ALTER TABLE `sg_group_defaults`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `sg_posts`
--
ALTER TABLE `sg_posts`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_id` (`user_id`,`group_id`),
  ADD KEY `user_id_2` (`user_id`),
  ADD KEY `group_id` (`group_id`);

--
-- Index pour la table `sg_requests`
--
ALTER TABLE `sg_requests`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `request_type_id` (`request_type_id`);

--
-- Index pour la table `sg_request_types`
--
ALTER TABLE `sg_request_types`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `sg_users`
--
ALTER TABLE `sg_users`
  ADD PRIMARY KEY (`id`),
  ADD KEY `group_default_id` (`group_default_id`);

--
-- Index pour la table `sg_user_groups`
--
ALTER TABLE `sg_user_groups`
  ADD PRIMARY KEY (`user_id`,`group_id`),
  ADD KEY `user_id` (`user_id`),
  ADD KEY `group_id` (`group_id`);

--
-- Index pour la table `sg_user_roles`
--
ALTER TABLE `sg_user_roles`
  ADD PRIMARY KEY (`user_id`) USING BTREE;

--
-- AUTO_INCREMENT pour les tables exportées
--

--
-- AUTO_INCREMENT pour la table `sg_availabilities`
--
ALTER TABLE `sg_availabilities`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT pour la table `sg_availability_types`
--
ALTER TABLE `sg_availability_types`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT pour la table `sg_comments`
--
ALTER TABLE `sg_comments`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;
--
-- AUTO_INCREMENT pour la table `sg_events`
--
ALTER TABLE `sg_events`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT pour la table `sg_event_types`
--
ALTER TABLE `sg_event_types`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT pour la table `sg_galleries`
--
ALTER TABLE `sg_galleries`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;
--
-- AUTO_INCREMENT pour la table `sg_groups`
--
ALTER TABLE `sg_groups`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT pour la table `sg_group_defaults`
--
ALTER TABLE `sg_group_defaults`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT pour la table `sg_posts`
--
ALTER TABLE `sg_posts`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT pour la table `sg_requests`
--
ALTER TABLE `sg_requests`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT pour la table `sg_request_types`
--
ALTER TABLE `sg_request_types`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT pour la table `sg_users`
--
ALTER TABLE `sg_users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;
--
-- Contraintes pour les tables exportées
--

--
-- Contraintes pour la table `sg_availabilities`
--
ALTER TABLE `sg_availabilities`
  ADD CONSTRAINT `sg_availabilities_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `sg_users` (`id`),
  ADD CONSTRAINT `sg_availabilities_ibfk_2` FOREIGN KEY (`availability_type_id`) REFERENCES `sg_availability_types` (`id`),
  ADD CONSTRAINT `sg_availabilities_ibfk_3` FOREIGN KEY (`group_id`) REFERENCES `sg_groups` (`id`);

--
-- Contraintes pour la table `sg_comments`
--
ALTER TABLE `sg_comments`
  ADD CONSTRAINT `sg_comments_ibfk_1` FOREIGN KEY (`post_id`) REFERENCES `sg_posts` (`id`),
  ADD CONSTRAINT `sg_comments_ibfk_2` FOREIGN KEY (`user_id`) REFERENCES `sg_users` (`id`);

--
-- Contraintes pour la table `sg_events`
--
ALTER TABLE `sg_events`
  ADD CONSTRAINT `sg_events_ibfk_1` FOREIGN KEY (`event_type_id`) REFERENCES `sg_event_types` (`id`),
  ADD CONSTRAINT `sg_events_ibfk_2` FOREIGN KEY (`request_id`) REFERENCES `sg_requests` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `sg_events_ibfk_3` FOREIGN KEY (`user_id`) REFERENCES `sg_users` (`id`),
  ADD CONSTRAINT `sg_events_ibfk_4` FOREIGN KEY (`group_id`) REFERENCES `sg_groups` (`id`);

--
-- Contraintes pour la table `sg_posts`
--
ALTER TABLE `sg_posts`
  ADD CONSTRAINT `sg_posts_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `sg_users` (`id`),
  ADD CONSTRAINT `sg_posts_ibfk_2` FOREIGN KEY (`group_id`) REFERENCES `sg_groups` (`id`);

--
-- Contraintes pour la table `sg_requests`
--
ALTER TABLE `sg_requests`
  ADD CONSTRAINT `sg_request_ibfk_1s` FOREIGN KEY (`request_type_id`) REFERENCES `sg_request_types` (`id`);

--
-- Contraintes pour la table `sg_user_groups`
--
ALTER TABLE `sg_user_groups`
  ADD CONSTRAINT `sg_user_groups_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `sg_users` (`id`),
  ADD CONSTRAINT `sg_user_groups_ibfk_2` FOREIGN KEY (`group_id`) REFERENCES `sg_groups` (`id`);

--
-- Contraintes pour la table `sg_user_roles`
--
ALTER TABLE `sg_user_roles`
  ADD CONSTRAINT `sg_user_roles_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `sg_users` (`id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
