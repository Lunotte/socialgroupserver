FROM java:8
VOLUME /target/
ADD target/jwt-demo-1.war app.war
ENV JAVA_OPTS=""
ENTRYPOINT exec java $JAVA_OPTS -Djava.security.egd=file:/dev/./urandom -war /app.war