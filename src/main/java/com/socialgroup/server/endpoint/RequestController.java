package com.socialgroup.server.endpoint;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.socialgroup.server.endpoint.dto.RequestDto;
import com.socialgroup.server.entity.Group;
import com.socialgroup.server.entity.User;
import com.socialgroup.server.entity.mapper.IRequestMapper;
import com.socialgroup.server.iservice.IRequestService;
import com.socialgroup.server.security.auth.JwtAuthenticationToken;
import com.socialgroup.server.service.DatabaseUserService;
import com.socialgroup.server.service.GroupService;

@RestController
public class RequestController {

	@Autowired
	private IRequestService requestService;

	@Autowired
	private DatabaseUserService databaseUserService;

	@Autowired
	private GroupService groupService;

	@Autowired
	IRequestMapper mapper;

	@RequestMapping(value="api/requests", method=RequestMethod.GET, produces=MediaType.APPLICATION_JSON_VALUE)
    public List<RequestDto> requests(final JwtAuthenticationToken token) {
		return mapper.requestToRequestDto(requestService.findAll());
    }

	@RequestMapping(value="api/request/{requestId}", method=RequestMethod.GET, produces=MediaType.APPLICATION_JSON_VALUE)
    public RequestDto request(final JwtAuthenticationToken token, @PathVariable final Long requestId) throws Exception {
		return mapper.requestToRequestDto(requestService.findById(requestId));
    }

	@RequestMapping(value = "api/requests/group/{groupId}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public List<RequestDto> requestsGroup(final JwtAuthenticationToken token, @PathVariable final Long groupId) throws Exception {
		final Group group = groupService.findById(groupId);
		return mapper.requestToRequestDto(requestService.findByGroup(group));
	}

	@RequestMapping(value = "api/requests/user/{userId}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public List<RequestDto> requestsUser(final JwtAuthenticationToken token, @PathVariable final Long userId) throws Exception {
		final User user = databaseUserService.findById(userId);
		return mapper.requestToRequestDto(requestService.findByUser(user));
	}
}
