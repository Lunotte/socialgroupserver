package com.socialgroup.server.endpoint;

import java.util.Collection;

import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.socialgroup.server.endpoint.dto.CommentDto;
import com.socialgroup.server.endpoint.dto.PublicationDto;
import com.socialgroup.server.facade.PublicationFacade;
import com.socialgroup.server.security.auth.JwtAuthenticationToken;

@RestController
public class PublicationController {

	private final PublicationFacade publicationFacade;

	public PublicationController(final PublicationFacade publicationFacade) {
		super();
		this.publicationFacade = publicationFacade;
	}

	@RequestMapping(value="api/publications", method=RequestMethod.GET, produces=MediaType.APPLICATION_JSON_VALUE)
    public Collection<PublicationDto> posts(final JwtAuthenticationToken token) throws Exception {
		return publicationFacade.getAllPublications(token);
    }

	@RequestMapping(value="api/publications/request/{requestId}/checked/{requestType}", method=RequestMethod.POST, produces=MediaType.APPLICATION_JSON_VALUE)
    public Collection<PublicationDto> checkedRequest(final JwtAuthenticationToken token, @PathVariable final Long requestId, @PathVariable final Long requestType) throws Exception {
		return publicationFacade.checkRequest(token, requestId, requestType);
    }

	@RequestMapping(value="api/publications/request/{requestId}", method=RequestMethod.POST, produces=MediaType.APPLICATION_JSON_VALUE)
    public Collection<PublicationDto> addRequestType(final JwtAuthenticationToken token, @PathVariable final Long requestId, @RequestBody final String requestType) throws Exception {
		return publicationFacade.addRequestType(token, requestId, requestType);
    }

	@RequestMapping(value="/api/publications/post", method=RequestMethod.POST, produces=MediaType.APPLICATION_JSON_VALUE)
    public Collection<PublicationDto> createPost(final JwtAuthenticationToken token, @RequestBody final CreatePostJson post) throws Exception {
		return this.publicationFacade.createPost(token, post);
    }

	@RequestMapping(value="/api/publications/post/{postId}", method=RequestMethod.PUT, produces=MediaType.APPLICATION_JSON_VALUE)
    public PublicationDto updatePost(final JwtAuthenticationToken token, @PathVariable final Long postId, @RequestBody final CreatePostJson post) throws Exception {
		return this.publicationFacade.updatePost(token, postId, post);
    }

	@RequestMapping(value="/api/publications/request/{requestId}", method=RequestMethod.DELETE, produces=MediaType.APPLICATION_JSON_VALUE)
    public boolean deleteRequest(@PathVariable final Long requestId) throws Exception {
		this.publicationFacade.deleteRequest(requestId);
		return true;
    }

	@RequestMapping(value="/api/publications/post/{postId}/comment", method=RequestMethod.PUT, produces=MediaType.APPLICATION_JSON_VALUE)
    public Collection<PublicationDto> updateCommentToPost(final JwtAuthenticationToken token, @PathVariable final Long postId, @RequestBody final CommentDto comment) throws Exception {
		return publicationFacade.updateCommentToPost(token, postId, comment);
    }

}
