package com.socialgroup.server.endpoint;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.socialgroup.server.endpoint.dto.GalleryDto;
import com.socialgroup.server.entity.Gallery;
import com.socialgroup.server.entity.mapper.IGalleryMapper;
import com.socialgroup.server.iservice.IGalleryService;
import com.socialgroup.server.security.auth.JwtAuthenticationToken;

@RestController
public class GalleryController{

	@Autowired
	private IGalleryMapper mapper;

	@Autowired
	private IGalleryService galleryService;

	@RequestMapping(value="api/galleries", method=RequestMethod.GET, produces=MediaType.APPLICATION_JSON_VALUE)
    public List<GalleryDto> galleries(final JwtAuthenticationToken token) {
		return mapper.galleryToGalleryDto(galleryService.findAll());
    }

	@RequestMapping(value="api/gallery/{galleryId}", method=RequestMethod.GET, produces=MediaType.APPLICATION_JSON_VALUE)
    public GalleryDto gallery(final JwtAuthenticationToken token, @PathVariable final Long galleryId) throws Exception {
		return mapper.galleryToGalleryDto(galleryService.findById(galleryId));
    }

	@RequestMapping(value="/api/gallery", method=RequestMethod.POST, produces=MediaType.APPLICATION_JSON_VALUE)
    public Gallery createGallery(@RequestBody final Gallery gallery) {
		galleryService.saveAndFlush(gallery);
		return gallery;
    }

	@RequestMapping(value="/api/gallery", method=RequestMethod.PUT, produces=MediaType.APPLICATION_JSON_VALUE)
    public Gallery updateGallery(@RequestBody final Gallery gallery) {
		galleryService.saveAndFlush(gallery);
		return gallery;
    }

	@RequestMapping(value="/api/gallery/{galleryId}", method=RequestMethod.DELETE)
    public boolean deleteGallery(@PathVariable final Long galleryId) {
		galleryService.delete(galleryId);
		return true;
    }
}
