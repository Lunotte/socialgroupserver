package com.socialgroup.server.endpoint.dto;

public class GalleryDto {

	private Long id;
    
    private String name;
    
    private String description;
    
    private String path;
    
    public GalleryDto() {
	}
    
	public GalleryDto(Long id, String name, String description, String path) {
		super();
		this.id = id;
		this.name = name;
		this.description = description;
		this.path = path;
	}
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getPath() {
		return path;
	}

	public void setPath(String path) {
		this.path = path;
	}

}
