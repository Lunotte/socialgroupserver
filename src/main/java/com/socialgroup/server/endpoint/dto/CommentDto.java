package com.socialgroup.server.endpoint.dto;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.socialgroup.server.entity.Comment;

public class CommentDto extends FeedDto {

	@JsonIgnore
	private PostDto post;
	
	private CommentDto masterComment;
	
	public CommentDto() {
		super();
	}
	
	public CommentDto(Comment comment) {
		super(comment);
		if(comment.getMasterComment() != null) {
			this.masterComment = new CommentDto(comment.getMasterComment());
		}
		//this.post = new PostDto(comment.getPost());
	}
	
	public CommentDto(CommentDto comment) {
		super();
		this.masterComment = comment;
	}

	public CommentDto(PostDto post) {
		super();
		this.post = post;
	}

	public PostDto getPost() {
		return post;
	}

	public void setPost(PostDto post) {
		this.post = post;
	}

	public CommentDto getMasterComment() {
		return masterComment;
	}

	public void setMasterComment(CommentDto masterComment) {
		this.masterComment = masterComment;
	}

}
