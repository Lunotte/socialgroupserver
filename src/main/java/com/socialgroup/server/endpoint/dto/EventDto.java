package com.socialgroup.server.endpoint.dto;

import java.time.ZonedDateTime;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.socialgroup.server.entity.Event;

public class EventDto {

	private Long id;

	private ZonedDateTime start;

	private ZonedDateTime end;

	// @JsonIgnore
	private PostDto post;

	private UserDto user;

	@JsonIgnore
	private GroupDto group;
	
	private ZonedDateTime created;
	
	private ZonedDateTime updated;

	public EventDto() {
		super();
	}

	public EventDto(Event event) {
		super();
		this.id = event.getId();
		this.start = event.getStart();
		this.end = event.getEnd();
		if (event.getPost() != null) {
			this.post = new PostDto(event.getPost());
		}
		this.user = new UserDto(event.getUser());
		this.group = new GroupDto(event.getGroup());
		this.created = event.getCreated();
		this.updated = event.getUpdated();
	}

	public EventDto(Long id, ZonedDateTime start, ZonedDateTime end, PostDto post, UserDto user, GroupDto group) {
		super();
		this.id = id;
		this.start = start;
		this.end = end;
		this.post = post;
		this.user = user;
		this.group = group;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public ZonedDateTime getStart() {
		return start;
	}

	public void setStart(ZonedDateTime start) {
		this.start = start;
	}

	public ZonedDateTime getEnd() {
		return end;
	}

	public void setEnd(ZonedDateTime end) {
		this.end = end;
	}

	public PostDto getPost() {
		return post;
	}

	public void setPost(PostDto post) {
		this.post = post;
	}

	public UserDto getUser() {
		return user;
	}

	public void setUser(UserDto user) {
		this.user = user;
	}

	public GroupDto getGroup() {
		return group;
	}

	public void setGroup(GroupDto group) {
		this.group = group;
	}

	public ZonedDateTime getCreated() {
		return created;
	}

	public void setCreated(ZonedDateTime created) {
		this.created = created;
	}

	public ZonedDateTime getUpdated() {
		return updated;
	}

	public void setUpdated(ZonedDateTime updated) {
		this.updated = updated;
	}

}
