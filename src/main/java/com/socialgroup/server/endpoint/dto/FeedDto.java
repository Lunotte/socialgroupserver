package com.socialgroup.server.endpoint.dto;

import java.time.ZonedDateTime;

import com.socialgroup.server.entity.Comment;
import com.socialgroup.server.entity.Feed;

public class FeedDto {

	protected Long id;

	protected String message;

	protected ZonedDateTime created;
	
	protected ZonedDateTime updated;

	protected UserDto user;
	
	private Long postId;

	public FeedDto() {
		super();
	}
	
	public FeedDto(Feed feed) {
		super();
		this.id = feed.getId();
		this.message = feed.getMessage();
		this.created = feed.getCreated();
		this.updated = feed.getUpdated();
		this.user = new UserDto(feed.getUser());
		if(feed instanceof Comment) {
			this.postId = ((Comment) feed).getPost().getId();
		}
	}

	public FeedDto(Long id, String message, UserDto user) {
		super();
		this.id = id;
		this.message = message;
		this.user = user;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public UserDto getUser() {
		return user;
	}

	public void setUser(UserDto user) {
		this.user = user;
	}

	public ZonedDateTime getCreated() {
		return created;
	}

	public void setCreated(ZonedDateTime created) {
		this.created = created;
	}

	public ZonedDateTime getUpdated() {
		return updated;
	}

	public void setUpdated(ZonedDateTime updated) {
		this.updated = updated;
	}

	public Long getPostId() {
		return postId;
	}

	public void setPostId(Long postId) {
		this.postId = postId;
	}

}
