package com.socialgroup.server.endpoint.dto;

import java.time.ZonedDateTime;
import java.util.HashSet;
import java.util.Set;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.socialgroup.server.entity.User;
import com.socialgroup.server.entity.UserGroup;
import com.socialgroup.server.entity.literals.UserRole;

public class UserDto {

	private Long id;
	
	private ZonedDateTime created;
	
	private ZonedDateTime updated;

	private String email;

	private String avatar;
	
	private String firstname;

	private String lastname;

	private String username;

	@JsonIgnore
	private String password;

	@JsonIgnore
	private Set<GroupDto> groups = new HashSet<>();
	
	@JsonIgnore
	private Set<UserRole> roles;

	@JsonIgnore
	private Set<EventDto> events = new HashSet<>();

	@JsonIgnore
	private Set<FeedDto> feeds = new HashSet<>();

	@JsonIgnore
	private GroupDto group;
	
	public UserDto() {
		super();
	}
	
	public UserDto(User user) {
		super();
		this.id = user.getId();
		this.email = user.getEmail();
		this.avatar = user.getAvatar();
		this.firstname = user.getFirstname();
		this.lastname = user.getLastname();
		this.username = user.getUsername();
		this.password = user.getPassword();
		for (UserGroup g : user.getUserGroups()) {
			this.groups.add(new GroupDto(g));
		}
		this.roles = user.getRoles();
		this.group = new GroupDto(user.getGroup());
		this.created = user.getCreated();
		this.updated = user.getUpdated();
	}

	public UserDto(Long id, String email, String avatar, String firstname, String lastname, String username,
			String password, Set<GroupDto> groups, Set<UserRole> roles, Set<EventDto> events, Set<FeedDto> feeds,
			GroupDto group) {
		super();
		this.id = id;
		this.email = email;
		this.avatar = avatar;
		this.firstname = firstname;
		this.lastname = lastname;
		this.username = username;
		this.password = password;
		this.groups = groups;
		this.roles = roles;
		this.events = events;
		this.feeds = feeds;
		this.group = group;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getAvatar() {
		return avatar;
	}

	public void setAvatar(String avatar) {
		this.avatar = avatar;
	}

	public String getFirstname() {
		return firstname;
	}

	public void setFirstname(String firstname) {
		this.firstname = firstname;
	}

	public String getLastname() {
		return lastname;
	}

	public void setLastname(String lastname) {
		this.lastname = lastname;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public Set<GroupDto> getGroups() {
		return groups;
	}

	public void setGroups(Set<GroupDto> groups) {
		this.groups = groups;
	}

	public Set<UserRole> getRoles() {
		return roles;
	}

	public void setRoles(Set<UserRole> roles) {
		this.roles = roles;
	}

	public Set<EventDto> getEvents() {
		return events;
	}

	public void setEvents(Set<EventDto> events) {
		this.events = events;
	}

	public GroupDto getGroup() {
		return group;
	}

	public void setGroup(GroupDto group) {
		this.group = group;
	}

	public Set<FeedDto> getFeeds() {
		return feeds;
	}

	public void setFeeds(Set<FeedDto> feeds) {
		this.feeds = feeds;
	}

	public ZonedDateTime getCreated() {
		return created;
	}

	public void setCreated(ZonedDateTime created) {
		this.created = created;
	}

	public ZonedDateTime getUpdated() {
		return updated;
	}

	public void setUpdated(ZonedDateTime updated) {
		this.updated = updated;
	}
	
}
