package com.socialgroup.server.endpoint.dto;

import java.time.ZonedDateTime;

import com.socialgroup.server.entity.Group;
import com.socialgroup.server.entity.UserGroup;

public class GroupDto {

	private Long id;

	private String name;
	
	private String picture;
	
	private ZonedDateTime connected;
	/*private Set<UserDto> users;
	private Set<EventDto> events;
	private Set<PostDto> posts;*/

	public GroupDto() {
	}
	
	public GroupDto(UserGroup group) {
		super();
		this.id = group.getGroup().getId();
		this.name = group.getGroup().getName();
		this.connected = group.getLastConnextion();
		this.picture = group.getGroup().getPicture();
	}
	
	public GroupDto(Group group) {
		super();
		this.id = group.getId();
		this.name = group.getName();
		this.picture = group.getPicture();
		this.connected = group.getUserGroups().stream().filter(ug -> ug.getId().getGroupId() == group.getId()).findFirst().get().getLastConnextion();
	}

	public GroupDto(Long id, String name, String picture) {
		super();
		this.id = id;
		this.name = name;
		this.picture = picture;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public ZonedDateTime getConnected() {
		return connected;
	}

	public void setConnected(ZonedDateTime connected) {
		this.connected = connected;
	}

	public String getPicture() {
		return picture;
	}

	public void setPicture(String picture) {
		this.picture = picture;
	}

}
