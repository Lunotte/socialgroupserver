package com.socialgroup.server.endpoint.dto;

import java.time.ZonedDateTime;

import com.socialgroup.server.entity.literals.AvailabilityType;

public class CreateAvailability {

	private final AvailabilityType type;
	private final ZonedDateTime start;
	private final ZonedDateTime end;
	
	public CreateAvailability() {
		this.type = null;
		this.start = null;
		this.end = null;
	}

	public CreateAvailability(AvailabilityType type, Long id, ZonedDateTime start, ZonedDateTime end) {
		super();
		this.type = type;
		this.start = start;
		this.end = end;
	}

	public AvailabilityType getType() {
		return type;
	}

	public ZonedDateTime getStart() {
		return start;
	}

	public ZonedDateTime getEnd() {
		return end;
	}

}
