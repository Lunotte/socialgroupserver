package com.socialgroup.server.endpoint;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.socialgroup.server.endpoint.dto.UserDto;
import com.socialgroup.server.entity.User;
import com.socialgroup.server.entity.mapper.IUserMapper;
import com.socialgroup.server.security.auth.JwtAuthenticationToken;
import com.socialgroup.server.security.model.UserContext;
import com.socialgroup.server.service.DatabaseUserService;

@RestController
public class ProfileEndpoint {

	Logger logger = LoggerFactory.getLogger(ProfileEndpoint.class);

	@Autowired
	private DatabaseUserService databaseUserService;

	@Autowired
	IUserMapper mapper;

	@RequestMapping(value = "/api/me", method = RequestMethod.GET)
	public @ResponseBody UserContext get(final JwtAuthenticationToken token) {
		return (UserContext) token.getPrincipal();
	}

	@RequestMapping(value = "api/info", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public List<UserDto> users(final JwtAuthenticationToken token) {
		return mapper.userToUserDto(databaseUserService.findAll());
	}

	@RequestMapping(value = "api/user/{userId}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public UserDto user(final JwtAuthenticationToken token, @PathVariable final Long userId) throws Exception {
		return mapper.userToUserDto(databaseUserService.findById(userId));
	}

	@RequestMapping(value = "api/info/{username}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public UserDto user(final JwtAuthenticationToken token, @PathVariable final String username) {
		return mapper.userToUserDto(databaseUserService.getByAUsername(username));
	}

	@RequestMapping(value = "/api/user", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	public User createUser(@RequestBody final User user) {
		logger.info("le user -> {}", user);
		databaseUserService.saveAndFlush(user);
		return user;
	}

	@RequestMapping(value = "/api/user", method = RequestMethod.PUT, produces = MediaType.APPLICATION_JSON_VALUE)
	public User updateUser(@RequestBody final User user) {
		databaseUserService.saveAndFlush(user);
		return user;
	}

	@RequestMapping(value = "/api/user/{userId}", method = RequestMethod.DELETE)
	public boolean deleteUser(@PathVariable final Long userId) {
		databaseUserService.delete(userId);
		return true;
	}
}
