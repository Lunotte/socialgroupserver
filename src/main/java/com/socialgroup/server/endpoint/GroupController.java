package com.socialgroup.server.endpoint;

import java.util.Collection;

import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.socialgroup.server.endpoint.dto.GroupDto;
import com.socialgroup.server.entity.Group;
import com.socialgroup.server.entity.literals.ChoiceYesNoEnum;
import com.socialgroup.server.entity.mapper.IGroupMapper;
import com.socialgroup.server.facade.GroupFacade;
import com.socialgroup.server.security.auth.JwtAuthenticationToken;
import com.socialgroup.server.service.GroupService;

@RestController
public class GroupController {

	private final IGroupMapper mapper;
	private final GroupService groupService;
	private final GroupFacade groupFacade;

	public GroupController(final IGroupMapper mapper, final GroupService groupService, final GroupFacade groupFacade) {
		super();
		this.mapper = mapper;
		this.groupService = groupService;
		this.groupFacade = groupFacade;
	}

	@GetMapping(value="api/groups", produces=MediaType.APPLICATION_JSON_VALUE)
    public Collection<GroupDto> userGroups(final JwtAuthenticationToken token) throws Exception {
		return this.groupFacade.findUserGroups(token);
    }

	@GetMapping(value="api/group/{groupId}", produces=MediaType.APPLICATION_JSON_VALUE)
    public GroupDto group(final JwtAuthenticationToken token, @PathVariable final Long groupId) throws Exception {
		return mapper.groupToGroupDto(groupService.findById(groupId));
    }

	/**
	 * Post new Group
	 * @param group
	 * @return
	 * @throws Exception
	 */
	@PostMapping(value="/api/group", produces=MediaType.APPLICATION_JSON_VALUE)
    public GroupDto createGroup(final JwtAuthenticationToken token, @RequestBody final GroupDto groupDto) throws Exception {
		return this.groupFacade.addGroup(token, groupDto);
    }

	/**
	 * Post update Group
	 * @param group
	 * @return
	 */
	@PutMapping(value="/api/group", produces=MediaType.APPLICATION_JSON_VALUE)
    public Group updateGroup(@RequestBody final Group group) {
		groupService.saveAndFlush(group);
		return group;
    }

	/**
	 * Get the group used by the user at this moment
	 * @param token
	 * @param groupId
	 * @return
	 * @throws Exception
	 */
	@PutMapping(value="/api/group/{groupId}/actif", produces=MediaType.APPLICATION_JSON_VALUE)
    public GroupDto updateGroupActif(final JwtAuthenticationToken token, @PathVariable final Long groupId) throws Exception {
		return this.groupFacade.updateGroupActif(token, groupId);
    }

	/**
	 * Defined default group for the user
	 * @param token
	 * @param groupId
	 * @return
	 * @throws Exception
	 */
	@PutMapping(value="/api/group/{groupId}/default", produces=MediaType.APPLICATION_JSON_VALUE)
    public GroupDto updateDefaultGroup(final JwtAuthenticationToken token, @PathVariable final Long groupId) throws Exception {
		return this.groupFacade.updateDefaultGroup(token, groupId);
    }

	@GetMapping(value="/api/group/{userId}/default", produces=MediaType.APPLICATION_JSON_VALUE)
    public GroupDto getDefaultGroup(final JwtAuthenticationToken token, @PathVariable final Long userId) throws Exception {
		return this.groupFacade.getDefaultGroup(token, userId);
    }

	@PutMapping(value="/api/group/{groupId}/vote", produces=MediaType.APPLICATION_JSON_VALUE)
    public GroupDto prepareDeleteGroup(final JwtAuthenticationToken token, @PathVariable final Long groupId) throws Exception {
		return this.groupFacade.prepareDeleteGroup(token, groupId);
    }

	@PutMapping(value="/api/group/{groupId}/vote/{vote}", produces=MediaType.APPLICATION_JSON_VALUE)
    public GroupDto prepareDeleteGroup(final JwtAuthenticationToken token, @PathVariable final Long groupId, @PathVariable final ChoiceYesNoEnum vote) throws Exception {
		return this.groupFacade.voteForDeleteGroup(token, groupId, vote);
    }

	/**
	 * DELETE delete group
	 * @param groupId
	 * @return
	 */
	@DeleteMapping(value="/api/group/{groupId}")
    public boolean deleteGroup(@PathVariable final Long groupId) {
		groupService.delete(groupId);
		return true;
    }
}
