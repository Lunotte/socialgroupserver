package com.socialgroup.server.endpoint;

import java.util.List;

import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.socialgroup.server.endpoint.dto.FeedDto;
import com.socialgroup.server.entity.mapper.IFeedMapper;
import com.socialgroup.server.iservice.IFeedService;

@RestController
public class LineController {
	
	private IFeedService feedService;
	
	private IFeedMapper mapper;

	public LineController(IFeedService feedService, IFeedMapper mapper) {
		super();
		this.feedService = feedService;
		this.mapper = mapper;
	}
	
	@RequestMapping(value="api/feeds", method=RequestMethod.GET, produces=MediaType.APPLICATION_JSON_VALUE)
    public List<FeedDto> comments() {
		return mapper.feedToFeedDto(feedService.findAll());
    }

}
