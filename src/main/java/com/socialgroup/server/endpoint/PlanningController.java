package com.socialgroup.server.endpoint;

import java.util.Collection;

import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.socialgroup.server.endpoint.dto.AvailabilityDto;
import com.socialgroup.server.endpoint.dto.CreateAvailability;
import com.socialgroup.server.endpoint.dto.PlanningDto;
import com.socialgroup.server.facade.PlanningFacade;
import com.socialgroup.server.security.auth.JwtAuthenticationToken;

@RestController
public class PlanningController {
	
	private PlanningFacade planningFacade;
	
	public PlanningController(PlanningFacade planningFacade) {
		this.planningFacade = planningFacade;
	}

	@RequestMapping(value="api/plannings", method=RequestMethod.GET, produces=MediaType.APPLICATION_JSON_VALUE)
    public Collection<PlanningDto> plannings(JwtAuthenticationToken token) throws Exception {
		return this.planningFacade.getAllPlannings(token);
    }
	
	@RequestMapping(value="api/plannings/availability", method=RequestMethod.POST, produces=MediaType.APPLICATION_JSON_VALUE)
    public AvailabilityDto createAvailability(JwtAuthenticationToken token, @RequestBody CreateAvailability availability) throws Exception {
		return this.planningFacade.postAvailability(token, availability);
    }
	
	@RequestMapping(value="api/plannings/availability/{id}", method=RequestMethod.PUT, produces=MediaType.APPLICATION_JSON_VALUE)
    public AvailabilityDto updateAvailability(JwtAuthenticationToken token, @PathVariable Long id, @RequestBody CreateAvailability availability) throws Exception {
		return this.planningFacade.updateAvailability(token, id, availability);
    }

	@RequestMapping(value = "api/plannings/availability/{eventId}", method = RequestMethod.DELETE)
	public boolean deleteAvailability(@PathVariable Long eventId) throws Exception {
		this.planningFacade.deleteAvailability(eventId);
		return true;
	}
}
