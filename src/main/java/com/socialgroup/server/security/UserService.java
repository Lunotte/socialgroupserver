package com.socialgroup.server.security;

import java.util.List;
import java.util.Optional;

import com.socialgroup.server.entity.User;

/**
 *
 * @author vladimir.stankovic
 *
 * Aug 17, 2016
 */
public interface UserService {
    public Optional<User> getByUsername(String username);

	List<User> findAll();

	User getByAUsername(String username);

	User findById(Long userId) throws Exception;
}
