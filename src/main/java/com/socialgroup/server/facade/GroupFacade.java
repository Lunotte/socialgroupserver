package com.socialgroup.server.facade;

import java.time.ZonedDateTime;
import java.util.Collection;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.springframework.stereotype.Service;

import com.socialgroup.server.common.Utils;
import com.socialgroup.server.endpoint.dto.GroupDto;
import com.socialgroup.server.entity.BlackListGroup;
import com.socialgroup.server.entity.ChoiceGroupBlackList;
import com.socialgroup.server.entity.Group;
import com.socialgroup.server.entity.User;
import com.socialgroup.server.entity.UserGroup;
import com.socialgroup.server.entity.UserGroupKey;
import com.socialgroup.server.entity.literals.ChoiceYesNoEnum;
import com.socialgroup.server.security.auth.JwtAuthenticationToken;
import com.socialgroup.server.service.BlackListGroupService;
import com.socialgroup.server.service.ChoiceGroupBlackListService;
import com.socialgroup.server.service.DatabaseUserService;
import com.socialgroup.server.service.GroupService;
import com.socialgroup.server.service.UtilsService;

@Service
public class GroupFacade {

	private final GroupService groupService;
	private final DatabaseUserService userService;
	private final ChoiceGroupBlackListService choiceGroupBlackListService;
	private final BlackListGroupService blackListGroupService;

	public GroupFacade(final GroupService groupService, final DatabaseUserService userService,
			final ChoiceGroupBlackListService choiceGroupBlackListService, final BlackListGroupService blackListGroupService) {
		super();
		this.groupService = groupService;
		this.userService = userService;
		this.choiceGroupBlackListService = choiceGroupBlackListService;
		this.blackListGroupService = blackListGroupService;
	}

	public List<GroupDto> findUserGroups(final JwtAuthenticationToken token) throws Exception {
		final User user = UtilsService.getUser(token);

		try {
			final Collection<Group> groups = this.groupService.findUserGroups(user);
			return groups.stream().map(GroupDto::new).collect(Collectors.toList());
    	} catch (final Exception e) {
    		throw new Exception("Une erreur est survenue pendant le chargement des groupes", e);
    	}
	}

	public GroupDto updateGroupActif(final JwtAuthenticationToken token, final Long groupId) throws Exception {
		final User user = UtilsService.getUser(token);

		try {
			final Optional<UserGroup> userGroup = user.getUserGroups().stream().filter(ug -> ug.getGroup().getId().equals(groupId)).findFirst();
			if(userGroup.isPresent()) {
				userGroup.get().setLastConnextion(ZonedDateTime.now());
			}
			this.userService.save(user);
			return this.getDefaultGroup(token, groupId);

		} catch (final Exception e) {
    		throw new Exception("Une erreur est survenue pendant le changement de groupe.", e);
    	}
	}

	public GroupDto addGroup(final JwtAuthenticationToken token, final GroupDto groupDto) throws Exception {
		final User user = UtilsService.getUser(token);

		Group group = new Group();
		group.setName(groupDto.getName());

		group = this.groupService.save(group);
		final UserGroup userGroup = new UserGroup(user, group);
		userGroup.setId(new UserGroupKey(user.getId(), group.getId()));
		user.addUserGroup(userGroup);
		this.userService.save(user);
		group.addUserGroup(userGroup);
		this.groupService.save(group);

		return this.updateGroupActif(token, group.getId());
	}

	public GroupDto updateDefaultGroup(final JwtAuthenticationToken token, final Long groupId) throws Exception {
		final User user = UtilsService.getUser(token);

		try {
			final Optional<UserGroup> userGroup = user.getUserGroups().stream().filter(ug -> ug.getGroup().getId().equals(groupId)).findFirst();
			if(userGroup.isPresent()) {
				user.setGroup(userGroup.get().getGroup());
			}
			return new GroupDto(userService.save(user).getGroup());

		} catch (final Exception e) {
    		throw new Exception("Une erreur est survenue pendant le changement de groupe.", e);
    	}
	}

	public GroupDto getDefaultGroup(final JwtAuthenticationToken token, final Long groupId) throws Exception {
		final User user = UtilsService.getUser(token);

		try {
			return new GroupDto(Utils.groupActif(user).get().getGroup());

		} catch (final Exception e) {
    		throw new Exception("Une erreur est survenue pendant le changement de groupe.", e);
    	}
	}


	/**
	 * Une demande de suppression d'un group a été demandée
	 * @param token
	 * @param groupId
	 * @return
	 * @throws Exception
	 */
	public GroupDto prepareDeleteGroup(final JwtAuthenticationToken token, final Long groupId) throws Exception {

		try {
			final User user = UtilsService.getUser(token);
			final Group group = this.groupService.findById(groupId);

			final BlackListGroup blackListGroup = new BlackListGroup(limitDate(), group);
			final ChoiceGroupBlackList choiceGroupBlackList = new ChoiceGroupBlackList(blackListGroup, user, ChoiceYesNoEnum.YES);

			this.blackListGroupService.save(blackListGroup);
			this.choiceGroupBlackListService.save(choiceGroupBlackList);

			return null;

		} catch (final Exception e) {
    		throw new Exception("Une erreur est survenue pendant le changement de groupe.", e);
    	}
	}

	/**
	 * Vote for remove a group
	 * @param token
	 * @param groupId Group concerned
	 * @param vote ChoiceYesNoEnum
	 * @return
	 * @throws Exception
	 */
	public GroupDto voteForDeleteGroup(final JwtAuthenticationToken token, final Long groupId, final ChoiceYesNoEnum vote) throws Exception {

		try {
			final User user = UtilsService.getUser(token);
			final Group group = this.groupService.findById(groupId);

			final Optional<BlackListGroup> blackListGroup = this.blackListGroupService.findByGroup(group);

			if(!blackListGroup.isPresent()) {
				throw new Exception("Le group n'a pas été trouvé.");
			}

			final long voted = blackListGroup.get().getChoiceGroupBlackList().stream().filter(c -> c.getUser().equals(user)).count();
			if(voted > 0) {
				throw new Exception("Vous avez déjà voté pour ce groupe.");
			}
			final ChoiceGroupBlackList choiceGroupBlackList = new ChoiceGroupBlackList(blackListGroup.get(), user, vote);

			this.choiceGroupBlackListService.save(choiceGroupBlackList);

			return null;

		} catch (final Exception e) {
    		throw new Exception("Une erreur est survenue pendant le vote.", e);
    	}

	}

	/**
	 * Vérifie tous les éléments en attente
	 * @param token
	 * @throws Exception
	 */
	public void checkBlackList() throws Exception {
		try {
			this.checkBlackListGroup();

		} catch (final Exception e) {
    		throw new Exception("Une erreur est survenue pendant l'analyse des données blacklistées.", e);
    	}
	}

	/**
	 * Supprime les éléments répondant à la condition
	 * @param token
	 * @throws Exception
	 */
	public void checkBlackListGroup() throws Exception {
		try {
			final Collection<BlackListGroup> tempBlackList = this.blackListGroupService.getAllBlackListTemporary();

			for (final BlackListGroup blackListGroup : tempBlackList) {
				if(blackListGroup.canBeBlackListed()) {
					this.blackListGroupService.delete(blackListGroup);
				}
			}

		} catch (final Exception e) {
    		throw new Exception("Une erreur est survenue pendant la suppression  du group.", e);
    	}
	}

	public GroupDto prepareRemoveUserFromGroup(final JwtAuthenticationToken token, final Long groupId, final Long userId) throws Exception {
		final User user = UtilsService.getUser(token);

		try {
			// TODO

		} catch (final Exception e) {
    		throw new Exception("Une erreur est survenue pendant le changement de groupe.", e);
    	}
		return null;
	}

	private ZonedDateTime limitDate() {
		ZonedDateTime date = ZonedDateTime.now();
		date = date.plusDays(7L);
		return date;
	}

}
