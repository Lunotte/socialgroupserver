package com.socialgroup.server.facade;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.springframework.stereotype.Service;

import com.socialgroup.server.endpoint.dto.AvailabilityDto;
import com.socialgroup.server.endpoint.dto.CreateAvailability;
import com.socialgroup.server.endpoint.dto.PlanningDto;
import com.socialgroup.server.endpoint.dto.RequestDto;
import com.socialgroup.server.entity.Availability;
import com.socialgroup.server.entity.Group;
import com.socialgroup.server.entity.Request;
import com.socialgroup.server.entity.User;
import com.socialgroup.server.security.UserService;
import com.socialgroup.server.security.auth.JwtAuthenticationToken;
import com.socialgroup.server.security.model.UserContext;
import com.socialgroup.server.service.AvailabilityService;
import com.socialgroup.server.service.RequestService;
import com.socialgroup.server.service.UtilsService;

@Service
public class PlanningFacade {
	
	private RequestService requestService;
	private AvailabilityService availabilityService;
	private UserService userService;
	
	public PlanningFacade(RequestService requestService, AvailabilityService availabilityService, UserService userService) {
		this.requestService = requestService;
		this.availabilityService = availabilityService;
		this.userService = userService;
	}
	
	public Collection<PlanningDto> getAllPlannings(JwtAuthenticationToken token) throws Exception {
		final User user = this.userService.findById(Long.valueOf(((UserContext) token.getPrincipal()).getUsername()));
		Group group = UtilsService.getGroup(user);
		
		List<PlanningDto> planningsDto = new ArrayList<>();
		
		try {
			List<Availability> availabilities =  this.availabilityService.findByGroup(group);
			List<Request> requests = this.requestService.findByGroupAndSurveyIsFalseOrderByCreatedDesc(group);
			
			for (Availability availability : availabilities) {
				planningsDto.add(new PlanningDto(new AvailabilityDto(availability)));
			}
			
			for (Request request : requests) {
				planningsDto.add(new PlanningDto(new RequestDto(request)));
			}
		} catch (Exception e) {
			throw new Exception("Un problème est survenu pendant la récupération des plannings", e);
		}
		
		return planningsDto;
	}
	
	/**
	 * Create a new Availability
	 * 
	 * @param token
	 * @param availability
	 * @return
	 * @throws Exception
	 */
	public AvailabilityDto postAvailability(JwtAuthenticationToken token, CreateAvailability availability) throws Exception {
		final User user = this.userService.findById(Long.valueOf(((UserContext) token.getPrincipal()).getUsername()));
		Group group = UtilsService.getGroup(user);
		
		try {
			
			List<Availability> availabilities = this.availabilityService.getAvailabilitiesBetweenStartAndEnd(
					availability.getStart(), availability.getEnd(), group, user);
			if(!availabilities.isEmpty()) {
				throw new Exception("An availability is already present.");
			}
			
			Availability anAvailability = new Availability(availability.getType());
			anAvailability.setGroup(group);
			anAvailability.setStart(availability.getStart());
			anAvailability.setEnd(availability.getEnd());
			anAvailability.setUser(user);
			
			return new AvailabilityDto(this.availabilityService.saveAndFlush(anAvailability));
			
		} catch (Exception e) {
			throw new Exception("Un problème est survenu pendant la création de l'event", e);
		}
	}
	
	public AvailabilityDto updateAvailability(JwtAuthenticationToken token, Long idAvailability, CreateAvailability availability) throws Exception {
		
		try {
			final User user = this.userService.findById(Long.valueOf(((UserContext) token.getPrincipal()).getUsername()));
			Group group = UtilsService.getGroup(user);
			
			List<Availability> availabilities = this.availabilityService.getAvailabilitiesBetweenStartAndEnd(
					availability.getStart(), availability.getEnd(), group, user);
			
			if((availabilities.size() == 1 && availabilities.get(0).getId() != idAvailability)|| availabilities.size() > 1) {
				throw new Exception("An availability is already present.");
			}
			
			Availability anAvailability = this.availabilityService.findById(idAvailability);
			anAvailability.setStart(availability.getStart());
			anAvailability.setEnd(availability.getEnd());
			anAvailability.setAvailabilityType(availability.getType());
			
			return new AvailabilityDto(this.availabilityService.saveAndFlush(anAvailability));
			
		} catch (Exception e) {
			throw new Exception("Un problème est survenu pendant la création de l'event", e);
		}
	}
	
	
	public void deleteAvailability(Long idAvailability) throws Exception {
		
		try {
			this.availabilityService.delete(idAvailability);
		} catch (Exception e) {
			throw new Exception("Un problème est survenu pendant la création de l'event", e);
		}
	}
}
