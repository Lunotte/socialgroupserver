package com.socialgroup.server.facade;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.Set;

import org.springframework.stereotype.Service;

import com.socialgroup.server.common.Utils;
import com.socialgroup.server.endpoint.CreatePostJson;
import com.socialgroup.server.endpoint.dto.CommentDto;
import com.socialgroup.server.endpoint.dto.PostDto;
import com.socialgroup.server.endpoint.dto.PublicationDto;
import com.socialgroup.server.endpoint.dto.RequestDto;
import com.socialgroup.server.entity.Comment;
import com.socialgroup.server.entity.CommentSurvey;
import com.socialgroup.server.entity.Group;
import com.socialgroup.server.entity.Post;
import com.socialgroup.server.entity.Request;
import com.socialgroup.server.entity.RequestType;
import com.socialgroup.server.entity.User;
import com.socialgroup.server.entity.UserGroup;
import com.socialgroup.server.facade.comparator.SortByDateCreated;
import com.socialgroup.server.security.UserService;
import com.socialgroup.server.security.auth.JwtAuthenticationToken;
import com.socialgroup.server.security.model.UserContext;
import com.socialgroup.server.service.CommentService;
import com.socialgroup.server.service.CommentSurveyService;
import com.socialgroup.server.service.PostService;
import com.socialgroup.server.service.RequestService;
import com.socialgroup.server.service.RequestTypeService;
import com.socialgroup.server.service.UtilsService;

@Service
public class PublicationFacade {

	private final PostService postService;
	private final RequestService requestService;
	private final RequestTypeService requestTypeService;
	private final CommentService commentService;
	private final CommentSurveyService commentSurveyService;
	private final UserService userService;

	public PublicationFacade(final PostService postService, final RequestService requestService, final RequestTypeService requestTypeService, final CommentService commentService,
			final CommentSurveyService commentSurveyService, final UserService userService) {
		super();
		this.postService = postService;
		this.requestService = requestService;
		this.requestTypeService = requestTypeService;
		this.commentService = commentService;
		this.commentSurveyService = commentSurveyService;
		this.userService = userService;
	}

	public List<PublicationDto> getAllPublications(final JwtAuthenticationToken token) throws Exception {

		final User user = this.userService.findById(Long.valueOf(((UserContext) token.getPrincipal()).getUsername()));

		final Optional<UserGroup> userGroup = Utils.groupActif(user);

		Group group = null;
		if(userGroup.isPresent()) {
			group = userGroup.get().getGroup();
		} else {
			throw new Exception("Un problème est survenu pour retrouver le group actif");
		}

		final List<Post> posts =  this.postService.findAllOrderByDate(group);
		final List<Request> requests = this.requestService.findAllOrderByDate(group);
		final List<PublicationDto> publicationsDto = new ArrayList<>();

		for (final Post post : posts) {
			publicationsDto.add(new PublicationDto(new PostDto(post)));
		}

		for (final Request request : requests) {
			final Optional<PublicationDto> publicationDto = publicationsDto.stream().filter(p -> request.getPost() != null && p.getPost().getId() == request.getPost().getId()).findFirst();
			if(publicationDto.isPresent()) {
				publicationsDto.remove(publicationDto.get());
				publicationsDto.add(new PublicationDto(new RequestDto(request)));
			}
		}

		Collections.sort(publicationsDto, new SortByDateCreated());

		return publicationsDto;
	}

	public Collection<PublicationDto> checkRequest(final JwtAuthenticationToken token, final Long requestId, final Long requestType) throws Exception{

		Utils.isNull(requestId);
		Utils.isNull(requestType);

		final Request request = this.requestService.findById(requestId);

		final User user = this.userService.findById(Long.valueOf(((UserContext) token.getPrincipal()).getUsername()));

		final Optional<CommentSurvey> comment = request.getCommentSurvey().stream()
				.filter(c ->
					c.getUser().getId().equals(user.getId())
					&& c.getRequestType().getId().equals(requestType)).findFirst();

		if(comment.isPresent()) {
			this.commentSurveyService.remove(comment.get());
			request.removeCommentSurvey(comment.get());
		} else {
			final CommentSurvey commentSurvey = new CommentSurvey();
			commentSurvey.setUser(user);
			commentSurvey.setRequest(request);
			commentSurvey.setRequestType(this.requestTypeService.findById(requestType));

			this.commentSurveyService.save(commentSurvey);
			request.addCommentSurvey(commentSurvey);
		}
		this.requestService.save(request);

		return getAllPublications(token);
	}

	public Collection<PublicationDto> createPost(final JwtAuthenticationToken token, final CreatePostJson post) throws Exception {
		if(post.getMessage().isEmpty()) {
			throw new Exception("Le message ne doit pas êter vide");
		}

		final User user = this.userService.findById(Long.valueOf(((UserContext) token.getPrincipal()).getUsername()));
		final Group group = UtilsService.getGroup(user);

		final Post poste = new Post();
		poste.setMessage(post.getMessage());
		poste.setUser(user);
		poste.setGroup(group);

		final Post createdPost = postService.saveAndFlush(poste);

		if(post.isRequest()) {

			final Set<RequestType> requestsType = this.requestTypeService.createRequestTypes(post.getRequestsType());
			final Request request = this.requestService.createRequests(createdPost, requestsType, post, user);

			this.requestTypeService.save(requestsType);
			this.requestService.save(request);
		}

		return getAllPublications(token);
	}

	public PublicationDto updatePost(final JwtAuthenticationToken token, final Long id, final CreatePostJson post) throws Exception {

		try {

			if(post.getMessage().isEmpty()) {
				throw new Exception("Le message ne doit pas êter vide");
			}

			final Request request = this.requestService.findById(id);
			request.getPost().setMessage(post.getMessage());
			request.setDescription(post.getMessage());
			request.setStart(post.getStart());
			request.setEnd(post.getEnd());

			request.removeAllRequestsType();
			final Set<RequestType> requestsType = this.requestTypeService.createRequestTypes(post.getRequestsType());

			for (final RequestType requestType : requestsType) {
				request.addRequestType(requestType);
			}

			this.requestTypeService.save(requestsType);
			this.requestService.save(request);

			return new PublicationDto(new RequestDto(request));

		}catch(final Exception e) {
			throw new Exception("Un problème est survenu pendant la mise à jour de l'event", e);
		}
	}

	public Collection<PublicationDto> addRequestType(final JwtAuthenticationToken token, final Long requestId, final String requestType) throws Exception{

		Utils.isNull(requestId);
		Utils.isEmpty(requestType);

		final Request request = this.requestService.findById(requestId);

		final RequestType newRequestType = new RequestType();
		newRequestType.setName(requestType);
		request.addRequestType(newRequestType);

		this.requestTypeService.save(newRequestType);
		this.requestService.save(request);

		return getAllPublications(token);
	}

	public Collection<PublicationDto> updateCommentToPost(final JwtAuthenticationToken token, final Long postId, final CommentDto dto) throws Exception {
		final Post post = postService.findById(postId);
		final User user = this.userService.findById(Long.valueOf(((UserContext) token.getPrincipal()).getUsername()));
		final Comment comment = new Comment();
    	comment.setMessage(dto.getMessage());
    	comment.setPost(post);
    	comment.setUser(user);
    	this.commentService.saveAndFlush(comment);
		return getAllPublications(token);
	}

	/**
	 * Supprimer une Request
	 * @param requestId
	 * @throws Exception
	 */
	public void deleteRequest(final Long requestId) throws Exception {
		try {
			this.requestService.delete(requestId);
		} catch (final Exception e) {
			throw new Exception("Un problème est survenu pendant la suppression de l'event", e);
		}
	}

}
