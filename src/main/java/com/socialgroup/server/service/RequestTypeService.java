package com.socialgroup.server.service;

import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.socialgroup.server.entity.RequestType;
import com.socialgroup.server.repository.RequestTypeRepository;

@Service
public class RequestTypeService {
    private final RequestTypeRepository requestTypeRepository;

    @Autowired
    public RequestTypeService(final RequestTypeRepository requestTypeRepository) {
        this.requestTypeRepository = requestTypeRepository;
    }

    public RequestTypeRepository getRequestTypeRepository() {
        return requestTypeRepository;
    }

    public List<RequestType> findAll() {
        return this.requestTypeRepository.findAll();
    }

    public RequestType findById(final Long requestTypeId) throws Exception {
    	final Optional<RequestType> requestsType = this.requestTypeRepository.findById(requestTypeId);
    	if(requestsType.isPresent()) {
    		return requestsType.get();
    	} else {
    		throw new Exception("L'identifiant n'a pas été trouvé");
    	}
    }

    public RequestType save(final RequestType requestType) throws Exception {
    	try {
    		return this.requestTypeRepository.save(requestType);
    	} catch (final Exception e) {
    		throw new Exception("Une erreur pendant l'insertion des requests type", e);
    	}
    }

    public List<RequestType> save(final Set<RequestType> requestsType) throws Exception {
    	try {
    		return this.requestTypeRepository.saveAll(requestsType);
    	} catch (final Exception e) {
    		throw new Exception("Une erreur pendant l'insertion des requests type", e);
    	}
    }

    public Set<RequestType> createRequestTypes(final Collection<String> requestTypes) {
    	final Set<RequestType> lstRequestsType = new HashSet<>();
		for (final String request : requestTypes) {
			final RequestType newRequest = new RequestType();
			newRequest.setName(request);
			lstRequestsType.add(newRequest);
		}

		return lstRequestsType;
    }
}