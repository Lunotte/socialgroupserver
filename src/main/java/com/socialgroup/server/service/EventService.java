package com.socialgroup.server.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.socialgroup.server.entity.Event;
import com.socialgroup.server.iservice.IEventService;
import com.socialgroup.server.repository.EventRepository;

@Service
public class EventService implements IEventService{
    private final EventRepository eventRepository;

    @Autowired
    public EventService(final EventRepository eventRepository) {
        this.eventRepository = eventRepository;
    }

    public EventRepository getEventRepository() {
        return eventRepository;
    }

    @Override
    public List<Event> findAll() {
        return this.eventRepository.findAll();
    }

    @Override
	public Optional<Event> findById(final Long eventId) {
		return this.eventRepository.findById(eventId);
	}

	@Override
    public Event saveAndFlush(final Event event){
    	return this.eventRepository.saveAndFlush(event);
    }

    @Override
	public void delete(final Long eventId) {
		this.eventRepository.deleteById(eventId);
	}

}