package com.socialgroup.server.service;

import java.util.List;
import java.util.Optional;

import org.springframework.stereotype.Service;

import com.socialgroup.server.entity.Comment;
import com.socialgroup.server.iservice.ICommentService;
import com.socialgroup.server.repository.CommentRepository;

@Service
public class CommentService implements ICommentService{
    private final CommentRepository commentRepository;

    public CommentService(final CommentRepository commentRepository) {
        this.commentRepository = commentRepository;
    }

    public CommentRepository getCommentRepository() {
        return commentRepository;
    }

    @Override
    public List<Comment> findAll() {
        return this.commentRepository.findAll();
    }

	@Override
	public Comment findById(final Long commentId) throws Exception {
		final Optional<Comment> comment = this.commentRepository.findById(commentId);
        if(comment.isPresent()) {
    		return comment.get();
    	} else {
    		throw new Exception("L'identifiant n'a pas été trouvé");
    	}
	}

	@Override
    public Comment saveAndFlush(final Comment comment){
    	return this.commentRepository.saveAndFlush(comment);
    }

    @Override
	public void delete(final Long commentId) {
		this.commentRepository.deleteById(commentId);
	}

}