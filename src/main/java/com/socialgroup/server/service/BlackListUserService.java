package com.socialgroup.server.service;

import java.util.List;

import org.springframework.stereotype.Service;

import com.socialgroup.server.entity.BlackListUser;
import com.socialgroup.server.repository.BlackListUserRepository;

@Service
public class BlackListUserService {

	private final BlackListUserRepository blackListUserRepository;

	public BlackListUserService(final BlackListUserRepository blackListUserRepository) {
		super();
		this.blackListUserRepository = blackListUserRepository;
	}

	/**
	 * Save or Update UserGroup
	 * @param userGroup
	 * @return UserGroup updated
	 * @throws Exception
	 */
	public BlackListUser save(final BlackListUser blackListUser) throws Exception{

		try {
			return this.blackListUserRepository.save(blackListUser);

		} catch (final Exception e) {
    		throw new Exception(String.format("Une erreur est survenue pendant la sauvegarde.", blackListUser), e);
    	}
    }

	public List<BlackListUser> getAllBlackListTemporary() throws Exception {
		try {
			return this.blackListUserRepository.findAll();
		} catch (final Exception e) {
    		throw new Exception(String.format("Une erreur est survenue pendant la récupération des blacklistes."), e);
    	}
	}

}
