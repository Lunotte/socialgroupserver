package com.socialgroup.server.service;

import java.util.Optional;

import org.springframework.stereotype.Service;

import com.socialgroup.server.common.Utils;
import com.socialgroup.server.entity.Group;
import com.socialgroup.server.entity.User;
import com.socialgroup.server.entity.UserGroup;
import com.socialgroup.server.security.auth.JwtAuthenticationToken;
import com.socialgroup.server.security.model.UserContext;

@Service
public class UtilsService {

	private static DatabaseUserService databaseUserService;
	
	public UtilsService(DatabaseUserService userService) {
		databaseUserService = userService;
	}
	
	public static User getUser(JwtAuthenticationToken token) throws Exception {
		final User user = databaseUserService.findById(Long.valueOf(((UserContext) token.getPrincipal()).getUsername()));
		
		if(user == null) {
			throw new Exception("L'utilisateur n'existe pas");
		}
		
		return user;
	}
	
	public static Group getGroup(User user) throws Exception {
		Optional<UserGroup> userGroup = Utils.groupActif(user);
		
		if(userGroup.isPresent()) {
			return userGroup.get().getGroup();
		} else {
			throw new Exception("Un problème est survenu pour retrouver le group actif");
		}
	}
}
