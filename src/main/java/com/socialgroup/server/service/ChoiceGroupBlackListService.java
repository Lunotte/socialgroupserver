package com.socialgroup.server.service;

import org.springframework.stereotype.Service;

import com.socialgroup.server.entity.ChoiceGroupBlackList;
import com.socialgroup.server.repository.ChoiceGroupBlackListRepository;

@Service
public class ChoiceGroupBlackListService {

	private final ChoiceGroupBlackListRepository choiceGroupBlackListRepository;

	public ChoiceGroupBlackListService(final ChoiceGroupBlackListRepository choiceGroupBlackListRepository) {
		super();
		this.choiceGroupBlackListRepository = choiceGroupBlackListRepository;
	}

	/**
	 * Save or Update ChoiceUserBlackList
	 * @param userGroup
	 * @return UserGroup updated
	 * @throws Exception
	 */
	public ChoiceGroupBlackList save(final ChoiceGroupBlackList choiceGroupBlackList) throws Exception{

		try {
			return this.choiceGroupBlackListRepository.save(choiceGroupBlackList);

		} catch (final Exception e) {
    		throw new Exception(String.format("Une erreur est survenue pendant la sauvegarde.", choiceGroupBlackList), e);
    	}
    }

}
