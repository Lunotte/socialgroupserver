package com.socialgroup.server.service;

import java.time.ZonedDateTime;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.socialgroup.server.entity.Availability;
import com.socialgroup.server.entity.Group;
import com.socialgroup.server.entity.User;
import com.socialgroup.server.iservice.IAvailabilityService;
import com.socialgroup.server.repository.AvailabilityRepository;

@Service
public class AvailabilityService implements IAvailabilityService{
    private final AvailabilityRepository availabilityRepository;

    @Autowired
    public AvailabilityService(final AvailabilityRepository availabilityRepository) {
        this.availabilityRepository = availabilityRepository;
    }

    public AvailabilityRepository getAvailabilityRepository() {
        return availabilityRepository;
    }

    @Override
    public List<Availability> findAll() {
        return this.availabilityRepository.findAll();
    }

	@Override
	public Availability findById(final Long availabilityId) throws Exception {
		final Optional<Availability> availability = this.availabilityRepository.findById(availabilityId);
        if(availability.isPresent()) {
    		return availability.get();
    	} else {
    		throw new Exception("L'identifiant n'a pas été trouvé");
    	}
	}

	@Override
	public List<Availability> findByUser(final User user) {
		return this.availabilityRepository.findByUser(user);
	}

	@Override
	public List<Availability> findByGroup(final Group group) {
		return this.availabilityRepository.findByGroupOrderByCreatedDesc(group);
	}

	@Override
    public Availability saveAndFlush(final Availability availability){
    	return this.availabilityRepository.saveAndFlush(availability);
    }

    @Override
	public void delete(final Long availabilityId) {
		this.availabilityRepository.deleteById(availabilityId);
	}

    public List<Availability> getAvailabilitiesBetweenStartAndEnd(final ZonedDateTime start, final ZonedDateTime end, final Group group, final User user) {
    	return this.availabilityRepository.findByStartLessThanEqualAndEndGreaterThanEqualAndGroupAndUser(end, start, group, user);
    }
}