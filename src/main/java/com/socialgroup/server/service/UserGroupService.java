package com.socialgroup.server.service;

import java.util.Optional;

import org.springframework.stereotype.Service;

import com.socialgroup.server.entity.Group;
import com.socialgroup.server.entity.User;
import com.socialgroup.server.entity.UserGroup;
import com.socialgroup.server.repository.UserGroupRepository;

@Service
public class UserGroupService {

	private final UserGroupRepository userGroupRepository;

	public UserGroupService(final UserGroupRepository userGroupRepository) {
		super();
		this.userGroupRepository = userGroupRepository;
	}

	public UserGroup getUserGroupByUserAndGroup(final User user, final Group group) throws Exception {

		try {
			final Optional<UserGroup> userGroup = this.userGroupRepository.findByGroupAndUser(group, user);

			if(userGroup.isPresent()) {
				return userGroup.get();
			} else {
				throw new Exception(String.format("L'utilisateur %d n'a pas été trouvé dans le groupe %d.", user.getId(), group.getId()));
			}

		} catch (final Exception e) {
    		throw new Exception("Une erreur est survenue pendant la recherche.", e);
    	}
	}

	/**
	 * Save or Update UserGroup
	 * @param userGroup
	 * @return UserGroup updated
	 * @throws Exception
	 */
	public UserGroup save(final UserGroup userGroup) throws Exception{

		try {
			return this.userGroupRepository.save(userGroup);

		} catch (final Exception e) {
    		throw new Exception(String.format("Une erreur est survenue pendant la sauvegarde.", userGroup.getId()), e);
    	}
    }

}
