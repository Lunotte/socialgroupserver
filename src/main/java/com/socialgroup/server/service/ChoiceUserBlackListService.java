package com.socialgroup.server.service;

import org.springframework.stereotype.Service;

import com.socialgroup.server.entity.ChoiceUserBlackList;
import com.socialgroup.server.repository.ChoiceUserBlackListRepository;

@Service
public class ChoiceUserBlackListService {

	private final ChoiceUserBlackListRepository choiceUserBlackListRepository;

	public ChoiceUserBlackListService(final ChoiceUserBlackListRepository choiceUserBlackListRepository) {
		super();
		this.choiceUserBlackListRepository = choiceUserBlackListRepository;
	}

	/**
	 * Save or Update ChoiceUserBlackList
	 * @param userGroup
	 * @return UserGroup updated
	 * @throws Exception
	 */
	public ChoiceUserBlackList save(final ChoiceUserBlackList choiceUserBlackList) throws Exception{

		try {
			return this.choiceUserBlackListRepository.save(choiceUserBlackList);

		} catch (final Exception e) {
    		throw new Exception(String.format("Une erreur est survenue pendant la sauvegarde.", choiceUserBlackList), e);
    	}
    }

}
