package com.socialgroup.server.service;

import java.util.List;

import org.springframework.stereotype.Service;

import com.socialgroup.server.entity.Feed;
import com.socialgroup.server.iservice.IFeedService;
import com.socialgroup.server.repository.FeedRepository;

@Service
public class FeedService implements IFeedService{

	private FeedRepository feedRepository;

	public FeedService(FeedRepository feedRepository) {
		super();
		this.feedRepository = feedRepository;
	}
	
	@Override
	public List<Feed> findAll(){
		return this.feedRepository.findAll();
	}
}
