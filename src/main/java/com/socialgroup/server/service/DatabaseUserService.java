package com.socialgroup.server.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.socialgroup.server.entity.User;
import com.socialgroup.server.repository.UserRepository;
import com.socialgroup.server.security.UserService;

/**
 * Mock implementation.
 *
 * @author vladimir.stankovic
 *
 * Aug 4, 2016
 */
@Service
public class DatabaseUserService implements UserService {
    private final UserRepository userRepository;

    @Autowired
    public DatabaseUserService(final UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    public UserRepository getUserRepository() {
        return userRepository;
    }

    @Override
    public User findById(final Long userId) throws Exception {
        final Optional<User> user = this.userRepository.findById(userId);
        if(user.isPresent()) {
    		return user.get();
    	} else {
    		throw new Exception("L'identifiant n'a pas été trouvé");
    	}
    }

    @Override
    public Optional<User> getByUsername(final String username) {
        return this.userRepository.findByUsername(username);
    }

    @Override
    public User getByAUsername(final String username) {
        return this.userRepository.findByAUsername(username);
    }

    @Override
    public List<User> findAll() {
        return this.userRepository.findAll();
    }

	public User saveAndFlush(final User user) {
		/*BCryptPasswordEncoder encoder = new BCryptPasswordEncoder();
		User u = new User();
		u.setEmail("");
		u.setAvatar("");
		u.setEmail("test@test.fr");
		u.setFirstname("test");
		u.setGroup(this.groupRepository.getOne(1L));
		u.setLastname("TEST");
		u.setPassword(encoder.encode("test"));
		u.setUsername("test");*/
		return this.userRepository.saveAndFlush(user);
	}

	public void delete(final Long userId) {
		this.userRepository.deleteById(userId);
	}

	public User save(final User user) {
		return this.userRepository.save(user);
	}
}
