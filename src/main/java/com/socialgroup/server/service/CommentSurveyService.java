package com.socialgroup.server.service;

import org.springframework.stereotype.Service;

import com.socialgroup.server.entity.CommentSurvey;
import com.socialgroup.server.repository.CommentSurveyRepository;

@Service
public class CommentSurveyService {

	private final CommentSurveyRepository commentSurveyRepository;

	public CommentSurveyService(CommentSurveyRepository commentSurveyRepository) {
		this.commentSurveyRepository = commentSurveyRepository;
	}

	public CommentSurvey save(CommentSurvey commentSurvey){
    	return this.commentSurveyRepository.saveAndFlush(commentSurvey);
    }
	
	public void remove(CommentSurvey commentSurvey){
    	this.commentSurveyRepository.delete(commentSurvey);
    }
}
