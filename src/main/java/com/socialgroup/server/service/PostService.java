package com.socialgroup.server.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.socialgroup.server.entity.Group;
import com.socialgroup.server.entity.Post;
import com.socialgroup.server.repository.PostRepository;

@Service
public class PostService{
    private final PostRepository postRepository;

    @Autowired
    public PostService(final PostRepository postRepository) {
        this.postRepository = postRepository;
    }

    public PostRepository getPostRepository() {
        return postRepository;
    }

    public List<Post> findAll() {
        return this.postRepository.findAll();
    }

    public List<Post> findAllOrderByDate(final Group group) {
        return this.postRepository.findByGroupOrderByCreatedDesc(group);
    }

	public Post findById(final Long postId) throws Exception {
		final Optional<Post> post = this.postRepository.findById(postId);
        if(post.isPresent()) {
    		return post.get();
    	} else {
    		throw new Exception("L'identifiant n'a pas été trouvé");
    	}
	}

    public Post saveAndFlush(final Post post){
    	return this.postRepository.saveAndFlush(post);
    }

   /* public Post updateCommentToPost(final Post post, final CommentDto commentDto){
    	Comment comment = new Comment();
    	comment.setMessage(commentDto.getMessage());
    	comment.setUser(post.getUser());
    	//comment.setCreated(LocalDateTime.now());
    	return this.postRepository.save(post);
    }*/

	public void delete(final Long postId) {
		this.postRepository.deleteById(postId);
	}

}