package com.socialgroup.server.service;

import java.util.Collection;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.socialgroup.server.entity.Group;
import com.socialgroup.server.entity.User;
import com.socialgroup.server.repository.GroupRepository;

@Service
public class GroupService {
    private final GroupRepository groupRepository;

    @Autowired
    public GroupService(final GroupRepository groupRepository) {
        this.groupRepository = groupRepository;
    }

    public GroupRepository getGroupRepository() {
        return groupRepository;
    }

    public Collection<Group> findAll() {
        return this.groupRepository.findAll();
    }

	public Group findById(final Long groupId) throws Exception {
		final Optional<Group> group = this.groupRepository.findById(groupId);
        if(group.isPresent()) {
    		return group.get();
    	} else {
    		throw new Exception("L'identifiant n'a pas été trouvé");
    	}
	}

    public Group saveAndFlush(final Group group){
    	return this.groupRepository.saveAndFlush(group);
    }

    public Group save(final Group group){
    	return this.groupRepository.save(group);
    }

	public void delete(final Long groupId) {
		this.groupRepository.deleteById(groupId);
	}

	public Collection<Group> findUserGroups(final User user) throws Exception {
		try {
			return this.groupRepository.findByUserGroupsUser(user);
    	} catch (final Exception e) {
    		throw new Exception("Une erreur pendant l'insertion des requests type", e);
    	}
	}
}