package com.socialgroup.server.service;

import java.util.List;
import java.util.Optional;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.socialgroup.server.endpoint.CreatePostJson;
import com.socialgroup.server.entity.Group;
import com.socialgroup.server.entity.Post;
import com.socialgroup.server.entity.Request;
import com.socialgroup.server.entity.RequestType;
import com.socialgroup.server.entity.User;
import com.socialgroup.server.iservice.IRequestService;
import com.socialgroup.server.repository.RequestRepository;

@Service
public class RequestService implements IRequestService{
    private final RequestRepository requestRepository;

    @Autowired
    public RequestService(final RequestRepository requestRepository) {
        this.requestRepository = requestRepository;
    }

    public RequestRepository getRequestRepository() {
        return requestRepository;
    }

    public List<Request> findAllOrderByDate(final Group group) {
        return this.requestRepository.findByGroupOrderByCreatedDesc(group);
    }

    public List<Request> findByGroupAndSurveyIsFalseOrderByCreatedDesc(final Group group) {
        return this.requestRepository.findByGroupAndSurveyIsFalseOrderByCreatedDesc(group);
    }

    @Override
    public List<Request> findAll() {
        return this.requestRepository.findAll();
    }

    @Override
    public Request findById(final Long requestId) throws Exception {
        final Optional<Request> request = this.requestRepository.findById(requestId);
        if(request.isPresent()) {
    		return request.get();
    	} else {
    		throw new Exception("L'identifiant n'a pas été trouvé");
    	}
    }

    @Override
	public List<Request> findByUser(final User user) {
		return this.requestRepository.findByUser(user);
	}

	@Override
	public List<Request> findByGroup(final Group group) {
		return this.requestRepository.findByGroup(group);
	}

	@Override
	public Request save(final Request request) {
		return this.requestRepository.save(request);
	}

	public void delete(final Long requestId) {
		this.requestRepository.deleteById(requestId);
	}

	public Request createRequests(final Post createdPost, final Set<RequestType> requestsType, final CreatePostJson post, final User user) {
		final Request request = new Request();
		request.setDescription(createdPost.getMessage());
		request.setRequestTypes(requestsType);
		request.setPost(createdPost);
		request.setUser(user);
		request.setGroup(user.getUserGroups().stream().findFirst().get().getGroup());

		if(post.isSurvey()) {
			request.setSurvey(true);
		} else {
			request.setStart(post.getStart());
			request.setEnd(post.getEnd());
		}

		return request;
    }
}