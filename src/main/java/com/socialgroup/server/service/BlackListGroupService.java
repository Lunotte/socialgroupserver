package com.socialgroup.server.service;

import java.time.ZonedDateTime;
import java.util.Collection;
import java.util.Optional;

import org.springframework.stereotype.Service;

import com.socialgroup.server.entity.BlackListGroup;
import com.socialgroup.server.entity.Group;
import com.socialgroup.server.repository.BlackListGroupRepository;

@Service
public class BlackListGroupService {

	private final BlackListGroupRepository blackListGroupRepository;

	public BlackListGroupService(final BlackListGroupRepository blackListGroupRepository) {
		super();
		this.blackListGroupRepository = blackListGroupRepository;
	}

	public Optional<BlackListGroup> findByGroup(final Group group) throws Exception{

		try {
			return this.blackListGroupRepository.findByGroup(group);

		} catch (final Exception e) {
    		throw new Exception("Une erreur est survenue pendant la sauvegarde du group.", e);
    	}
    }

	/**
	 * Save or Update BlackListGroup
	 * @param userGroup
	 * @return UserGroup updated
	 * @throws Exception
	 */
	public BlackListGroup save(final BlackListGroup blackListGroup) throws Exception{

		try {
			return this.blackListGroupRepository.save(blackListGroup);

		} catch (final Exception e) {
    		throw new Exception("Une erreur est survenue pendant la sauvegarde du group.", e);
    	}
    }


	public Collection<BlackListGroup> getAllBlackListTemporary() throws Exception {
		try {
			return this.blackListGroupRepository.findAllByDateLimitBefore(ZonedDateTime.now());
		} catch (final Exception e) {
    		throw new Exception("Une erreur est survenue pendant la récupération des blacklistes.", e);
    	}
	}

	public void delete(final BlackListGroup blackListGroup) throws Exception {
		try {
			this.blackListGroupRepository.delete(blackListGroup);
		} catch (final Exception e) {
    		throw new Exception("Une erreur est survenue pendant la récupération des blacklistes.", e);
    	}
	}

}
