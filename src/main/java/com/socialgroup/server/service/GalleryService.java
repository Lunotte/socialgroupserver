package com.socialgroup.server.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.socialgroup.server.entity.Gallery;
import com.socialgroup.server.iservice.IGalleryService;
import com.socialgroup.server.repository.GalleryRepository;

@Service
public class GalleryService implements IGalleryService {
    private final GalleryRepository galleryRepository;

    @Autowired
    public GalleryService(final GalleryRepository galleryRepository) {
        this.galleryRepository = galleryRepository;
    }

    public GalleryRepository getGalleryRepository() {
        return galleryRepository;
    }

    @Override
    public List<Gallery> findAll() {
        return this.galleryRepository.findAll();
    }

	@Override
	public Gallery findById(final Long galleryId) throws Exception {
		final Optional<Gallery> gallery = this.galleryRepository.findById(galleryId);
        if(gallery.isPresent()) {
    		return gallery.get();
    	} else {
    		throw new Exception("L'identifiant n'a pas été trouvé");
    	}
	}

	@Override
    public Gallery saveAndFlush(final Gallery gallery){
    	return this.galleryRepository.saveAndFlush(gallery);
    }

    @Override
	public void delete(final Long galleryId) {
		this.galleryRepository.deleteById(galleryId);
	}
}