package com.socialgroup.server.cron;

import org.springframework.stereotype.Component;

import com.socialgroup.server.facade.GroupFacade;

@Component
public class CronTask {

	private final GroupFacade groupFacade;

	public CronTask(final GroupFacade groupFacade) {
		this.groupFacade = groupFacade;
	}

//	@Scheduled(fixedDelay = 1000)
//	public void scheduleFixedDelayTask() throws Exception {
//		this.groupFacade.checkBlackList();
//	}

}
