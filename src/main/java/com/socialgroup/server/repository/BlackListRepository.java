package com.socialgroup.server.repository;

import java.time.ZonedDateTime;
import java.util.Collection;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.NoRepositoryBean;

import com.socialgroup.server.entity.Group;

@NoRepositoryBean
public interface BlackListRepository<T> extends JpaRepository<T, Long>{

	Collection<T> findAllByDateLimitBefore(ZonedDateTime date);

	Optional<T> findByGroup(Group group);
}
