package com.socialgroup.server.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.socialgroup.server.entity.CommentSurvey;

@Repository
public interface CommentSurveyRepository extends JpaRepository<CommentSurvey, Long>{

}
