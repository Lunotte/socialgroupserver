package com.socialgroup.server.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.socialgroup.server.entity.ChoiceGroupBlackList;

@Repository
public interface ChoiceGroupBlackListRepository extends CrudRepository<ChoiceGroupBlackList, Long>{

}
