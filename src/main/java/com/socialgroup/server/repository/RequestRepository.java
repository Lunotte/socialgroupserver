package com.socialgroup.server.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.query.Param;

import com.socialgroup.server.entity.Group;
import com.socialgroup.server.entity.Request;
import com.socialgroup.server.entity.User;

public interface RequestRepository extends JpaRepository<Request, Long> {

    @Override
	public List<Request> findAll();
    @Override
	public Optional<Request> findById(@Param("id") Long requestId);
    public List<Request> findByUser(User user);
	public List<Request> findByGroup(Group group);
	public List<Request> findByGroupOrderByCreatedDesc(Group group);
	public List<Request> findByGroupAndSurveyIsFalseOrderByCreatedDesc(Group group);

}

