package com.socialgroup.server.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.query.Param;

import com.socialgroup.server.entity.Group;
import com.socialgroup.server.entity.Post;

public interface PostRepository extends JpaRepository<Post, Long> {

    @Override
	public List<Post> findAll();
    @Override
	public Optional<Post> findById(@Param("id") Long postId);
    public List<Post> findByGroupOrderByCreatedDesc(Group group);

}

