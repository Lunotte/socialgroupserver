package com.socialgroup.server.repository;

import java.util.Collection;
import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.query.Param;

import com.socialgroup.server.entity.Group;
import com.socialgroup.server.entity.User;

public interface GroupRepository extends JpaRepository<Group, Long> {

    @Override
	public List<Group> findAll();
    @Override
	public Optional<Group> findById(@Param("id") Long groupId);
    public Collection<Group> findByUserGroupsUser(User user);
}

