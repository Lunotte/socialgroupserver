package com.socialgroup.server.repository;

import org.springframework.stereotype.Repository;

import com.socialgroup.server.entity.BlackListGroup;

@Repository
public interface BlackListGroupRepository extends BlackListRepository<BlackListGroup>{

}
