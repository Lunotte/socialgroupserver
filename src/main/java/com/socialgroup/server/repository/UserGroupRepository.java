package com.socialgroup.server.repository;

import java.util.Optional;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.socialgroup.server.entity.Group;
import com.socialgroup.server.entity.User;
import com.socialgroup.server.entity.UserGroup;

@Repository
public interface UserGroupRepository extends CrudRepository<UserGroup, Long>{

	Optional<UserGroup> findByGroupAndUser(Group group, User user);
}
