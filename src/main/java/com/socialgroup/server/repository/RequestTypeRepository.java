package com.socialgroup.server.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.query.Param;

import com.socialgroup.server.entity.RequestType;

public interface RequestTypeRepository extends JpaRepository<RequestType, Long> {

    @Override
	public List<RequestType> findAll();
    @Override
	public Optional<RequestType> findById(@Param("id") Long requestTypeId);

}

