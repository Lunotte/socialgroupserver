package com.socialgroup.server.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.socialgroup.server.entity.Feed;

public interface FeedRepository extends JpaRepository<Feed, Long> {
	
}
