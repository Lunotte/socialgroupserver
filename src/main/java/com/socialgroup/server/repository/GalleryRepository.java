package com.socialgroup.server.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.query.Param;

import com.socialgroup.server.entity.Gallery;

public interface GalleryRepository extends JpaRepository<Gallery, Long> {

    @Override
	public List<Gallery> findAll();
    @Override
	public Optional<Gallery> findById(@Param("id") Long galleryId);

}

