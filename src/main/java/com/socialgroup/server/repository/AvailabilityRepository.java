package com.socialgroup.server.repository;

import java.time.ZonedDateTime;
import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.socialgroup.server.entity.Availability;
import com.socialgroup.server.entity.Group;
import com.socialgroup.server.entity.User;

public interface AvailabilityRepository extends JpaRepository<Availability, Long> {

    @Override
	public List<Availability> findAll();
    @Override
	public Optional<Availability> findById(@Param("id") Long availabilityId);

    @Query("select a from Availability a join a.user u where u =:user")
    public List<Availability> findByUser(@Param("user") User user);

    public List<Availability> findByGroupOrderByCreatedDesc(Group group);

    public List<Availability> findByStartLessThanEqualAndEndGreaterThanEqualAndGroupAndUser(ZonedDateTime end, ZonedDateTime start, Group group, User user);
}

