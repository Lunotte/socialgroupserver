package com.socialgroup.server.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.socialgroup.server.entity.Event;

public interface EventRepository extends JpaRepository<Event, Long> {
   
   /* public List<Event> findAll();
    public Event findById(@Param("id") Long eventId);
    @Query("select e from Event e join e.user u where u =:user")
    public List<Event> findByUser(@Param("user") User user);
    
    @Query("select e from Event e join e.group g where g =:group")
    public List<Event> findByGroup(@Param("group") Group group);*/
}

