package com.socialgroup.server.iservice;

import java.util.List;

import com.socialgroup.server.entity.Comment;

public interface ICommentService {

	List<Comment> findAll();
	Comment findById(Long commentId) throws Exception;
	void delete(Long commentId);
	Comment saveAndFlush(Comment comment);
}
