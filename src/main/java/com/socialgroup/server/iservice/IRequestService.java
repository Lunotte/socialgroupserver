package com.socialgroup.server.iservice;

import java.util.List;

import com.socialgroup.server.entity.Group;
import com.socialgroup.server.entity.Request;
import com.socialgroup.server.entity.User;

public interface IRequestService {

	List<Request> findAll();
	Request findById(Long requestId) throws Exception;
	List<Request> findByUser(User user);
	List<Request> findByGroup(Group group);
	Request save(Request request);
}
