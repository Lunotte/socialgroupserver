package com.socialgroup.server.iservice;

import java.util.List;

import com.socialgroup.server.entity.Gallery;

public interface IGalleryService {

	List<Gallery> findAll();
	Gallery findById(Long galleryId) throws Exception;
	void delete(Long galleryId);
	Gallery saveAndFlush(Gallery gallery);
}
