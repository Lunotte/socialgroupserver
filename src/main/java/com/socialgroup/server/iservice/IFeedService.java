package com.socialgroup.server.iservice;

import java.util.List;

import com.socialgroup.server.entity.Feed;

public interface IFeedService {

	List<Feed> findAll();

}
