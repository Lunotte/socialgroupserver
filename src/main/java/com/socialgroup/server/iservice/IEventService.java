package com.socialgroup.server.iservice;

import java.util.List;
import java.util.Optional;

import com.socialgroup.server.entity.Event;

public interface IEventService {

	List<Event> findAll();
	Event saveAndFlush(Event event);
	void delete(Long eventId);
	Optional<Event> findById(Long eventId);
}
