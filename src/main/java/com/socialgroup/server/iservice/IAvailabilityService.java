package com.socialgroup.server.iservice;

import java.util.List;

import com.socialgroup.server.entity.Availability;
import com.socialgroup.server.entity.Group;
import com.socialgroup.server.entity.User;

public interface IAvailabilityService {

	List<Availability> findAll();
	Availability findById(Long availabilityId) throws Exception;
	List<Availability> findByUser(User user);
	List<Availability> findByGroup(Group group);
	Availability saveAndFlush(Availability availability);
	void delete(Long availabilityId);
}
