package com.socialgroup.server.common;

import java.util.Comparator;
import java.util.Optional;

import com.socialgroup.server.entity.User;
import com.socialgroup.server.entity.UserGroup;

public class Utils {
	
	public static void isNull(Object object) throws Exception {
		if(object == null) {
			throw new Exception("L'objet est null");
		}
	}
	
	public static void isEmpty(String object) throws Exception {
		if(object.isEmpty()) {
			throw new Exception("L'objet est vide");
		}
	}
	
	public static Optional<UserGroup> groupActif(User user) {
		return user.getUserGroups().stream()
									.max(Comparator.comparing(UserGroup::getLastConnextion));
	}
	
}
