package com.socialgroup.server.entity;

import java.io.Serializable;
import java.time.ZonedDateTime;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;

@MappedSuperclass
public abstract class BlackList extends AbstractDate implements Serializable{

	private static final long serialVersionUID = 216168523419728988L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(updatable = false, nullable = false, unique = true)
    protected Long id;

    @Column(name="date_limit", nullable=false)
    protected ZonedDateTime dateLimit;

	public BlackList(final ZonedDateTime dateLimit) {
		super();
		this.dateLimit = dateLimit;
	}

	public BlackList() {
	}

	public abstract boolean canBeBlackListed();

	public Long getId() {
		return id;
	}

	public void setId(final Long id) {
		this.id = id;
	}

	public ZonedDateTime getDateLimit() {
		return dateLimit;
	}

	public void setDateLimit(final ZonedDateTime dateLimit) {
		this.dateLimit = dateLimit;
	}

}
