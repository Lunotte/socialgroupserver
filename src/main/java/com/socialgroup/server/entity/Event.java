package com.socialgroup.server.entity;

import java.io.Serializable;
import java.time.ZonedDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import com.socialgroup.server.endpoint.dto.AvailabilityDto;
import com.socialgroup.server.endpoint.dto.EventDto;

@Entity
@Table(name="sg_events")
@Inheritance(strategy=InheritanceType.JOINED)
public abstract class Event extends AbstractDate implements Serializable{
	
	private static final long serialVersionUID = 4706073162284515350L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(updatable = false, nullable = false, unique = true)
    protected Long id;
    
    @Column(name="start_time", nullable=true)
    protected ZonedDateTime start;
    
    @Column(name="end_time", nullable=true)
    protected ZonedDateTime end;
    
    @OneToOne
    protected Post post;
    
    @ManyToOne
    @JoinColumn(name="user_id")
    protected User user;
    
    @ManyToOne
    @JoinColumn(name="group_id")
    protected Group group;
    
   /* @Column(name="created", nullable=false)
    @Temporal(TemporalType.TIMESTAMP)
	protected ZonedDateTime created;
	
	@Column(name="updated", nullable=false)
    @Temporal(TemporalType.TIMESTAMP)
	protected ZonedDateTime updated;*/
    
	public Event() {
		super();
	}
	
	public Event(Event event) {
		super();
		this.id = event.getId();
		this.start = event.getStart();
		this.end = event.getEnd();
		this.user = new User(event.getUser());
		this.group = new Group(event.getGroup());
		/*this.created = event.getCreated();
		this.updated = event.getUpdated();*/
	}
	
	public Event(EventDto eventDto) {
		super();
		this.id = eventDto.getId();
		this.start = eventDto.getStart();
		this.end = eventDto.getEnd();
		this.user = new User(eventDto.getUser());
		this.group = new Group(eventDto.getGroup());
	}
	
	public Event(AvailabilityDto availabilityDto) {
		super();
		this.id = availabilityDto.getId();
		this.start = availabilityDto.getStart();
		this.end = availabilityDto.getEnd();
		this.user = new User(availabilityDto.getUser());
		this.group = new Group(availabilityDto.getGroup());
	}
	
	public Event(Long id, ZonedDateTime start, ZonedDateTime end, User user, Group group) {
		super();
		this.id = id;
		this.start = start;
		this.end = end;
		this.user = user;
		this.group = group;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public ZonedDateTime getStart() {
		return start;
	}

	public void setStart(ZonedDateTime start) {
		this.start = start;
	}

	public ZonedDateTime getEnd() {
		return end;
	}

	public void setEnd(ZonedDateTime end) {
		this.end = end;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public Group getGroup() {
		return group;
	}

	public void setGroup(Group group) {
		this.group = group;
	}

	public Post getPost() {
		return post;
	}

	public void setPost(Post post) {
		this.post = post;
	}

	/*public ZonedDateTime getCreated() {
		return created;
	}

	public void setCreated(ZonedDateTime created) {
		this.created = created;
	}

	public ZonedDateTime getUpdated() {
		return updated;
	}

	public void setUpdated(ZonedDateTime updated) {
		this.updated = updated;
	}
*/
}
