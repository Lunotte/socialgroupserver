package com.socialgroup.server.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.socialgroup.server.entity.ChoiceGroupBlackList.ChoiceGroupBlackListId;
import com.socialgroup.server.entity.literals.ChoiceYesNoEnum;

@Entity
@Table(name="sg_group_black_list")
@IdClass(ChoiceGroupBlackListId.class)
public class ChoiceGroupBlackList implements Serializable{

	private static final long serialVersionUID = 8054666447829311639L;

	@Id
	@ManyToOne(fetch = FetchType.LAZY)
	private BlackListGroup blackListGroup;

	@Id
	@ManyToOne()
	private User user;

	@Column
	private ChoiceYesNoEnum choice;

	public ChoiceGroupBlackList() {
		super();
	}


	public ChoiceGroupBlackList(final BlackListGroup blackListGroup, final User user, final ChoiceYesNoEnum choice) {
		this.blackListGroup = blackListGroup;
		this.user = user;
		this.choice = choice;
	}

	public BlackListGroup getBlackListGroup() {
		return blackListGroup;
	}

	public User getUser() {
		return user;
	}

	public ChoiceYesNoEnum getChoice() {
		return choice;
	}

	public static class ChoiceGroupBlackListId implements Serializable {

		private static final long serialVersionUID = 3844627547372142840L;

		private Long blackListGroup;
        private Long user;

        public ChoiceGroupBlackListId() {
        	super();
        }

        public ChoiceGroupBlackListId(final Long blackListGroup, final Long user) {
            this.blackListGroup = blackListGroup;
            this.user = user;
        }

		@Override
		public int hashCode() {
			final int prime = 31;
			int result = 1;
			result = prime * result + ((blackListGroup == null) ? 0 : blackListGroup.hashCode());
			result = prime * result + ((user == null) ? 0 : user.hashCode());
			return result;
		}

		@Override
		public boolean equals(final Object obj) {
			if (this == obj) {
				return true;
			}
			if (obj == null) {
				return false;
			}
			if (getClass() != obj.getClass()) {
				return false;
			}
			final ChoiceGroupBlackListId other = (ChoiceGroupBlackListId) obj;
			if (blackListGroup == null) {
				if (other.blackListGroup != null) {
					return false;
				}
			} else if (!blackListGroup.equals(other.blackListGroup)) {
				return false;
			}
			if (user == null) {
				if (other.user != null) {
					return false;
				}
			} else if (!user.equals(other.user)) {
				return false;
			}
			return true;
		}


   }
}
