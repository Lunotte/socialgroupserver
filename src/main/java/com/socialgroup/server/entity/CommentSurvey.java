package com.socialgroup.server.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "sg_comments_survey")
public class CommentSurvey extends AbstractDate implements Serializable{

	private static final long serialVersionUID = 6497917989843965483L;

	@Id @Column(name="id")
    @GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;
	
	@ManyToOne
    @JoinColumn(name="request_id")
	private Request request;
	
	@ManyToOne
    @JoinColumn(name = "request_type_id")
	private RequestType requestType;
	
	@ManyToOne()
    @JoinColumn(name="user_id")
	private User user;
	
	/*@ManyToMany(cascade = CascadeType.MERGE)
	@JoinTable(name = "sg_commentSurvey_requestTypes", joinColumns = @JoinColumn(name = "commentSurvey_id", referencedColumnName = "id"), inverseJoinColumns = @JoinColumn(name = "requestType_id", referencedColumnName = "id"))
	*/
	/*@OneToMany(mappedBy = "requestType")
	private Set<CommentSurveyRequestType> requestType = new HashSet<>();*/

	public CommentSurvey() {
		super();
	}

	public CommentSurvey(Long id, User user, RequestType requestType, Request request) {
		super();
		this.id = id;
		this.user = user;
		this.requestType = requestType;
		this.request = request;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	/*public Set<CommentSurveyRequestType> getRequestType() {
		return requestType;
	}

	public void setRequestType(Set<CommentSurveyRequestType> requestType) {
		this.requestType = requestType;
	}*/

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public Request getRequest() {
		return request;
	}

	public void setRequest(Request request) {
		this.request = request;
	}

	public RequestType getRequestType() {
		return requestType;
	}

	public void setRequestType(RequestType requestType) {
		this.requestType = requestType;
	}	
	
}
