package com.socialgroup.server.entity;

import java.time.ZonedDateTime;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.socialgroup.server.entity.literals.ChoiceYesNoEnum;

@Entity
@Table(name="sg_black_list_group")
public class BlackListGroup extends BlackList{

	private static final long serialVersionUID = 5654891132844326837L;

	@ManyToOne(cascade = CascadeType.REMOVE)
	@JoinColumn(name = "group_id", nullable = false)
	private Group group;

	@OneToMany(mappedBy="blackListGroup", fetch=FetchType.EAGER, cascade = CascadeType.REMOVE)
	private Set<ChoiceGroupBlackList> choiceGroupBlackList = new HashSet<>();

	public BlackListGroup() {
		super();
	}

	public BlackListGroup(final ZonedDateTime dateLimit, final Group group) {
		super(dateLimit);
		this.group = group;
	}

	public Group getGroup() {
		return group;
	}

	public void setGroup(final Group group) {
		this.group = group;
	}

	public Set<ChoiceGroupBlackList> getChoiceGroupBlackList() {
		return choiceGroupBlackList;
	}

	public void setChoiceGroupBlackList(final Set<ChoiceGroupBlackList> choiceGroupBlackList) {
		this.choiceGroupBlackList = choiceGroupBlackList;
	}

	@Override
	public boolean canBeBlackListed() {
		final long choiceYes = this.getChoiceGroupBlackList().stream().filter(c -> c.getChoice().equals(ChoiceYesNoEnum.YES)).count();
		final long choiceNo = this.getChoiceGroupBlackList().stream().filter(c -> c.getChoice().equals(ChoiceYesNoEnum.NO)).count();

		return (choiceYes > choiceNo);
	}

}
