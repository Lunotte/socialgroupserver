package com.socialgroup.server.entity;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.socialgroup.server.endpoint.dto.GroupDto;

@Entity
@Table(name = "sg_groups")
public class Group implements Serializable{

	private static final long serialVersionUID = -932631652204586962L;

	@Id
	@Column(name = "id")
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;

	@Column(name = "name")
	private String name;

	@Column(name = "picture")
	private String picture;

	@OneToMany(mappedBy = "group", cascade = CascadeType.ALL)
	private Set<UserGroup> userGroups = new HashSet<>();

	@OneToMany(mappedBy = "group", cascade = CascadeType.REMOVE)
	private Set<Post> posts = new HashSet<>();

	public Group() {
	}

	public Group(final GroupDto groupDto) {
		this.id = groupDto.getId();
		this.name = groupDto.getName();
		this.picture = groupDto.getPicture();
		this.userGroups = null;
		this.posts = null;
	}

	public Group(final Long id, final String name, final String picture, final Set<UserGroup> users, final Set<Event> events, final Set<Post> posts) {
		this.id = id;
		this.name = name;
		this.picture = picture;
		this.userGroups = users;
		this.posts = posts;
	}

	public Group(final Group group) {
		this.id = group.getId();
		this.name = group.getName();
		this.picture = group.getPicture();
		this.userGroups = group.getUserGroups();
		this.posts = group.getPosts();
	}

	public Long getId() {
		return id;
	}

	public void setId(final Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(final String name) {
		this.name = name;
	}

	public Set<UserGroup> getUserGroups() {
		return userGroups;
	}

	public void setUsers(final Set<UserGroup> userGroups) {
		this.userGroups = userGroups;
	}

	public void addUserGroup(final UserGroup userGroup) {
		if(!this.userGroups.contains(userGroup)) {
			this.userGroups.add(userGroup);
		}
	}

	/*public Set<Event> getEvents() {
		return events;
	}

	public void setEvents(Set<Event> events) {
		this.events = events;
	}*/

	public Set<Post> getPosts() {
		return posts;
	}

	public void setPosts(final Set<Post> posts) {
		this.posts = posts;
	}

	public String getPicture() {
		return picture;
	}

	public void setPicture(final String picture) {
		this.picture = picture;
	}

}
