package com.socialgroup.server.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.socialgroup.server.endpoint.dto.FeedDto;

@Entity
@Table(name="sg_feeds")
@Inheritance(strategy=InheritanceType.JOINED)
public abstract class Feed extends AbstractDate implements Serializable{

	private static final long serialVersionUID = 1169875344888431425L;

	@Id @Column(name="id")
    @GeneratedValue(strategy = GenerationType.AUTO)
	protected Long id;
	
	@Column(name="message")
	protected String message;
	
	/*@Column(name="created", nullable=false)
    @Temporal(TemporalType.TIMESTAMP)
	protected ZonedDateTime created;
	
	@Column(name="updated", nullable=false)
    @Temporal(TemporalType.TIMESTAMP)
	protected ZonedDateTime updated;*/
	
	@ManyToOne
    @JoinColumn(name="user_id")
	protected User user;
	
	public Feed() {
		super();
	}
	
	public Feed(FeedDto feedDto) {
		this.id = feedDto.getId();
		this.message = feedDto.getMessage();
		/*this.created = feedDto.getCreated();
		this.updated = feedDto.getUpdated();*/
		this.user = new User(feedDto.getUser());
	}

	public Feed(Long id, String message, User user) {
		super();
		this.id = id;
		this.message = message;
		this.user = user;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	/*public ZonedDateTime getCreated() {
		return created;
	}

	public void setCreated(ZonedDateTime created) {
		this.created = created;
	}

	public ZonedDateTime getUpdated() {
		return updated;
	}

	public void setUpdated(ZonedDateTime updated) {
		this.updated = updated;
	}*/

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}
	
}
