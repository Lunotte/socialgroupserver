package com.socialgroup.server.entity;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import com.socialgroup.server.endpoint.dto.FeedDto;

@Entity
@Table(name = "sg_comments")
public class Comment extends Feed {

	private static final long serialVersionUID = -1971396695562910981L;

	@OneToOne(cascade = CascadeType.PERSIST)
    private Comment masterComment;
	
	@ManyToOne
	@JoinColumn(name = "post_id")
	private Post post;
	
	@Column(name="sub_comment", nullable=false)
	private boolean subComment = false;
	
	public Comment() {
	}

	public Comment(Long id, String message) {
		super();
	}

	public Comment(FeedDto feedDto) {
		super(feedDto);
	}

	public Post getPost() {
		return post;
	}

	public void setPost(Post post) {
		this.post = post;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public Comment getMasterComment() {
		return masterComment;
	}

	public void setMasterComment(Comment masterComment) {
		this.masterComment = masterComment;
	}

	public boolean isSubComment() {
		return subComment;
	}

	public void setSubComment(boolean subComment) {
		this.subComment = subComment;
	}

}
