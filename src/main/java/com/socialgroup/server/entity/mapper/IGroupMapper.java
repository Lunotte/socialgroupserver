package com.socialgroup.server.entity.mapper;

import java.util.List;

import com.socialgroup.server.endpoint.dto.GroupDto;
import com.socialgroup.server.entity.Group;

public interface IGroupMapper{

	GroupDto groupToGroupDto(Group group);
	Group groupDtoToGroup(GroupDto groupDto);
	
	List<GroupDto> groupToGroupDto(List<Group> group);
	List<Group> groupDtoToGroup(List<GroupDto> groupDto);
}
