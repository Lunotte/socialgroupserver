package com.socialgroup.server.entity.mapper.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Service;

import com.socialgroup.server.endpoint.dto.AvailabilityDto;
import com.socialgroup.server.entity.Availability;
import com.socialgroup.server.entity.mapper.IAvailabilityMapper;

@Service
public class AvailabilityMapper implements IAvailabilityMapper {

	@Override
	public AvailabilityDto availabilityToAvailabilityDto(Availability availability) {
		return new AvailabilityDto(availability);
	}

	@Override
	public Availability availabilityDtoToAvailability(AvailabilityDto availabilityDto) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<AvailabilityDto> availabilityToAvailabilityDto(List<Availability> availabilities) {
		List<AvailabilityDto> lstAvailabilities = new ArrayList<>();
		for (Availability availability : availabilities) {
			lstAvailabilities.add(new AvailabilityDto(availability));
		}
		return lstAvailabilities;
	}

}
