package com.socialgroup.server.entity.mapper.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Service;

import com.socialgroup.server.endpoint.dto.GalleryDto;
import com.socialgroup.server.entity.Gallery;
import com.socialgroup.server.entity.mapper.IGalleryMapper;

@Service
public class GalleryMapper implements IGalleryMapper{

	@Override
	public GalleryDto galleryToGalleryDto(Gallery gallery) {

		GalleryDto galleryDto = new GalleryDto();
		galleryDto.setId(gallery.getId());
		galleryDto.setName(gallery.getName());
		galleryDto.setDescription(gallery.getDescription());
		galleryDto.setPath(gallery.getPath());

		return galleryDto;
	}

	@Override
	public Gallery galleryDtoToGallery(GalleryDto galleryDto) {

		Gallery gallery = new Gallery();
		gallery.setId(galleryDto.getId());
		gallery.setName(galleryDto.getName());
		gallery.setDescription(galleryDto.getDescription());
		gallery.setPath(galleryDto.getPath());

		return gallery;
	}

	@Override
	public List<GalleryDto> galleryToGalleryDto(List<Gallery> gallery) {

		List<GalleryDto> lstGalleryDto = new ArrayList<>();

		for (Gallery g : gallery) {
			GalleryDto galleryDto = new GalleryDto();
			galleryDto.setId(g.getId());
			galleryDto.setName(g.getName());
			galleryDto.setDescription(g.getDescription());
			galleryDto.setPath(g.getPath());

			lstGalleryDto.add(galleryDto);
		}

		return lstGalleryDto;
	}

	@Override
	public List<Gallery> galleryDtoToGallery(List<GalleryDto> galleryDto) {

		List<Gallery> lstGallery = new ArrayList<>();

		for (GalleryDto g : galleryDto) {
			Gallery gallery = new Gallery();
			gallery.setId(g.getId());
			gallery.setName(g.getName());
			gallery.setDescription(g.getDescription());
			gallery.setPath(g.getPath());

			lstGallery.add(gallery);
		}

		return lstGallery;
	}

}
