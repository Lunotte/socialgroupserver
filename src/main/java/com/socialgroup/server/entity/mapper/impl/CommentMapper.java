package com.socialgroup.server.entity.mapper.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Service;

import com.socialgroup.server.endpoint.dto.CommentDto;
import com.socialgroup.server.entity.Comment;
import com.socialgroup.server.entity.mapper.ICommentMapper;

@Service
public class CommentMapper implements ICommentMapper {

	@Override
	public CommentDto commentToCommentDto(Comment comment) {
		return new CommentDto(comment);
	}
	
	@Override
	public List<CommentDto> commentToCommentDto(List<Comment> comments) {
		List<CommentDto> lstComments = new ArrayList<>();
		for (Comment comment : comments) {
			lstComments.add(new CommentDto(comment));
		}
		return lstComments;
	}

	@Override
	public Comment commentDtoToComment(CommentDto commentDto) {
		// TODO Auto-generated method stub
		return null;
	}

}
