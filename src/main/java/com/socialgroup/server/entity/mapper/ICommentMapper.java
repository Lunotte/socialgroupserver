package com.socialgroup.server.entity.mapper;

import java.util.List;

import com.socialgroup.server.endpoint.dto.CommentDto;
import com.socialgroup.server.entity.Comment;

public interface ICommentMapper {

	CommentDto commentToCommentDto(Comment comment);
	Comment commentDtoToComment(CommentDto commentDto);
	List<CommentDto> commentToCommentDto(List<Comment> comments);
}
