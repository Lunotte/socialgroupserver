package com.socialgroup.server.entity.mapper.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Service;

import com.socialgroup.server.endpoint.dto.UserDto;
import com.socialgroup.server.entity.User;
import com.socialgroup.server.entity.mapper.IUserMapper;

@Service
public class UserMapper implements IUserMapper {

	@Override
	public UserDto userToUserDto(User user) {
		return new UserDto(user);
	}
	
	@Override
	public List<UserDto> userToUserDto(List<User> users) {
		List<UserDto> lstUsers = new ArrayList<>();
		for (User user : users) {
			lstUsers.add(new UserDto(user));
		}
		return lstUsers;
	}

	@Override
	public User userDtoToUser(UserDto userDto) {
		// TODO Auto-generated method stub
		return null;
	}

}
