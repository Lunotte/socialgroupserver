package com.socialgroup.server.entity.mapper;

import java.util.List;

import com.socialgroup.server.endpoint.dto.RequestDto;
import com.socialgroup.server.entity.Request;

public interface IRequestMapper {

	RequestDto requestToRequestDto(Request request);
	Request requestDtoToRequest(RequestDto requestDto);
	List<RequestDto> requestToRequestDto(List<Request> requests);
}
