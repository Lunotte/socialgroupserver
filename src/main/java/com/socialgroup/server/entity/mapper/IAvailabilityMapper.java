package com.socialgroup.server.entity.mapper;

import java.util.List;

import com.socialgroup.server.endpoint.dto.AvailabilityDto;
import com.socialgroup.server.entity.Availability;

public interface IAvailabilityMapper {

    AvailabilityDto availabilityToAvailabilityDto(Availability availability);
    List<AvailabilityDto> availabilityToAvailabilityDto(List<Availability> availabilities);
    Availability availabilityDtoToAvailability(AvailabilityDto availabilityDto);
}
