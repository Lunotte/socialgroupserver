package com.socialgroup.server.entity.mapper.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Service;

import com.socialgroup.server.endpoint.dto.RequestDto;
import com.socialgroup.server.entity.Request;
import com.socialgroup.server.entity.mapper.IRequestMapper;

@Service
public class RequestMapper implements IRequestMapper {

	@Override
	public RequestDto requestToRequestDto(Request request) {
		return new RequestDto(request);
	}
	
	@Override
	public List<RequestDto> requestToRequestDto(List<Request> requests) {
		List<RequestDto> lstRequests = new ArrayList<>();
		for (Request request : requests) {
			lstRequests.add(new RequestDto(request));
		}
		return lstRequests;
	}

	@Override
	public Request requestDtoToRequest(RequestDto requestDto) {
		// TODO Auto-generated method stub
		return null;
	}

}
