package com.socialgroup.server.entity.mapper.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Service;

import com.socialgroup.server.endpoint.dto.GroupDto;
import com.socialgroup.server.entity.Group;
import com.socialgroup.server.entity.mapper.IGroupMapper;

@Service
public class GroupMapper implements IGroupMapper {

	@Override
	public GroupDto groupToGroupDto(Group group) {
		
		GroupDto groupDto = new GroupDto();
		groupDto.setId(group.getId());
		groupDto.setName(group.getName());

		return groupDto;
	}

	@Override
	public Group groupDtoToGroup(GroupDto groupDto) {
		
		Group group = new Group();
		group.setId(groupDto.getId());
		group.setName(groupDto.getName());

		return group;
	}

	@Override
	public List<GroupDto> groupToGroupDto(List<Group> group) {
		List<GroupDto> lstGroupDto = new ArrayList<>();

		for (Group g : group) {
			GroupDto groupDto = new GroupDto();
			groupDto.setId(g.getId());
			groupDto.setName(g.getName());

			lstGroupDto.add(groupDto);
		}

		return lstGroupDto;
	}

	@Override
	public List<Group> groupDtoToGroup(List<GroupDto> groupDto) {
		List<Group> lstGroup = new ArrayList<>();

		for (GroupDto g : groupDto) {
			Group group = new Group();
			group.setId(g.getId());
			group.setName(g.getName());

			lstGroup.add(group);
		}

		return lstGroup;
	}

}
