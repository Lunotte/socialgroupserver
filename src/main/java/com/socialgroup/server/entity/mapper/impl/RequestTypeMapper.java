package com.socialgroup.server.entity.mapper.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Service;

import com.socialgroup.server.endpoint.dto.RequestTypeDto;
import com.socialgroup.server.entity.RequestType;
import com.socialgroup.server.entity.mapper.IRequestTypeMapper;

@Service
public class RequestTypeMapper implements IRequestTypeMapper {

	@Override
	public RequestTypeDto requestTypeToRequestTypeDto(RequestType requestType) {
		return new RequestTypeDto(requestType);
	}
	
	@Override
	public List<RequestTypeDto> requestTypeToRequestTypeDto(List<RequestType> requestTypes) {
		List<RequestTypeDto> lstRequestTypes = new ArrayList<>();
		for (RequestType requestType : requestTypes) {
			lstRequestTypes.add(new RequestTypeDto(requestType));
		}
		return lstRequestTypes;
	}

	@Override
	public RequestType requestTypeDtoToRequestType(RequestTypeDto requestTypeDto) {
		return new RequestType(requestTypeDto);
	}

}
