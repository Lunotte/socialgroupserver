package com.socialgroup.server.entity.mapper;

import java.util.List;

import com.socialgroup.server.endpoint.dto.RequestTypeDto;
import com.socialgroup.server.entity.RequestType;

public interface IRequestTypeMapper {

	RequestTypeDto requestTypeToRequestTypeDto(RequestType requestType);
	RequestType requestTypeDtoToRequestType(RequestTypeDto requestTypeDto);
	List<RequestTypeDto> requestTypeToRequestTypeDto(List<RequestType> requestTypes);
}
