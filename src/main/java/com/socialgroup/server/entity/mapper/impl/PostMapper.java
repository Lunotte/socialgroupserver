package com.socialgroup.server.entity.mapper.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Service;

import com.socialgroup.server.endpoint.dto.PostDto;
import com.socialgroup.server.entity.Post;
import com.socialgroup.server.entity.mapper.IPostMapper;

@Service
public class PostMapper implements IPostMapper {

	@Override
	public PostDto postToPostDto(Post post) {
		return new PostDto(post);
	}

	@Override
	public List<PostDto> postToPostDto(List<Post> posts) {
		List<PostDto> lstPosts = new ArrayList<>();
		for (Post post : posts) {
			lstPosts.add(new PostDto(post));
		}
		return lstPosts;
	}
	
	@Override
	public Post postDtoToPost(PostDto postDto) {
		// TODO Auto-generated method stub
		return null;
	}

}
