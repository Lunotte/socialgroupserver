package com.socialgroup.server.entity.mapper.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Service;

import com.socialgroup.server.endpoint.dto.FeedDto;
import com.socialgroup.server.entity.Feed;
import com.socialgroup.server.entity.mapper.IFeedMapper;

@Service
public class FeedMapper implements IFeedMapper{
	
	@Override
	public FeedDto feedToFeedDto(Feed feed) {
		return new FeedDto(feed);
	}
	
	@Override
	public List<FeedDto> feedToFeedDto(List<Feed> feeds) {
		List<FeedDto> lstFeeds = new ArrayList<>();
		for (Feed feed : feeds) {
			lstFeeds.add(new FeedDto(feed));
		}
		return lstFeeds;
	}
}
