package com.socialgroup.server.entity.mapper;

import java.util.List;

import com.socialgroup.server.endpoint.dto.PostDto;
import com.socialgroup.server.entity.Post;

public interface IPostMapper {

	PostDto postToPostDto(Post post);
	Post postDtoToPost(PostDto postDto);
	List<PostDto> postToPostDto(List<Post> posts);
}
