package com.socialgroup.server.entity.mapper;

import java.util.List;

import com.socialgroup.server.endpoint.dto.FeedDto;
import com.socialgroup.server.entity.Feed;

public interface IFeedMapper {

	FeedDto feedToFeedDto(Feed feed);

	List<FeedDto> feedToFeedDto(List<Feed> feeds);

}
