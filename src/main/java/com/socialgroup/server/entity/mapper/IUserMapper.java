package com.socialgroup.server.entity.mapper;

import java.util.List;

import com.socialgroup.server.endpoint.dto.UserDto;
import com.socialgroup.server.entity.User;

public interface IUserMapper {

	UserDto userToUserDto(User user);
	User userDtoToUser(UserDto userDto);
	List<UserDto> userToUserDto(List<User> users);
}
