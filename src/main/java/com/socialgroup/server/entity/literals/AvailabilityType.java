package com.socialgroup.server.entity.literals;

public enum AvailabilityType{

	AVAILABLE("Available"),
	UNAVAILABLE("Unavailable");
	
   private final String label;

   AvailabilityType(String label)
   {
      this.label = label;
   }

   public String getLabel()
   {
      return this.label;
   }

   public static AvailabilityType fromString(String label)
   {
      for (AvailabilityType ae : AvailabilityType.values())
      {
         if (ae.label.equalsIgnoreCase(label))
         {
            return ae;
         }
      }
      return null;
   }
}
