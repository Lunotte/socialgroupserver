package com.socialgroup.server.entity;

import java.time.ZonedDateTime;
import java.util.Set;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.socialgroup.server.entity.literals.ChoiceYesNoEnum;

@Entity
@Table(name="sg_black_list_user")
public class BlackListUser extends BlackList{

	private static final long serialVersionUID = 5654891132844326837L;

	@ManyToOne
	@JoinColumn(name = "user_id", nullable = false)
	private User user;

	@ManyToOne
	@JoinColumn(name = "group_id", nullable = false)
	private Group group;

	@OneToMany(mappedBy="blackList")
	private Set<ChoiceUserBlackList> choiceUserBlackList;

	public BlackListUser() {
		super();
	}

	public BlackListUser(final ZonedDateTime dateLimit, final Group group, final User user) {
		super(dateLimit);
		this.group = group;
		this.user = user;
	}

	public User getUser() {
		return user;
	}

	public void setUser(final User user) {
		this.user = user;
	}

	public Group getGroup() {
		return group;
	}

	public void setGroup(final Group group) {
		this.group = group;
	}

	public Set<ChoiceUserBlackList> getChoiceUserBlackList() {
		return choiceUserBlackList;
	}

	public void setChoiceUserBlackList(final Set<ChoiceUserBlackList> choiceUserBlackList) {
		this.choiceUserBlackList = choiceUserBlackList;
	}

	@Override
	public boolean canBeBlackListed() {
		final long choiceYes = this.getChoiceUserBlackList().stream().filter(c -> c.getChoice().equals(ChoiceYesNoEnum.YES)).count();
		final long choiceNo = this.getChoiceUserBlackList().stream().filter(c -> c.getChoice().equals(ChoiceYesNoEnum.NO)).count();

		return (choiceYes > choiceNo);
	}

}
