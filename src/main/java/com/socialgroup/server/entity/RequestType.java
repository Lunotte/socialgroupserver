package com.socialgroup.server.entity;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.socialgroup.server.endpoint.dto.RequestTypeDto;

@Entity
@Table(name="sg_request_types")
public class RequestType {
	
	@Id @Column(name="id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    
    @Column(name="name")
    private String name;
    
    /*@JsonIgnore
    @OneToMany(mappedBy = "requestType")
    private Set<Request> requests = new HashSet<>();*/
    //@JsonIgnore
    @ManyToOne(cascade = CascadeType.MERGE)
	private Request request;
    
	public RequestType() {
	}

	public RequestType(Long id, String name) {
		this.id = id;
		this.name = name;
	}

	public RequestType(RequestTypeDto requestTypeDto) {
		this.setId(requestTypeDto.getId());
		this.setName(requestTypeDto.getName());
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Request getRequests() {
		return request;
	}

	public void setRequests(Request request) {
		this.request = request;
	}
    
}
