package com.socialgroup.server.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.socialgroup.server.entity.ChoiceUserBlackList.ChoiceUserBlackListId;
import com.socialgroup.server.entity.literals.ChoiceYesNoEnum;

@Entity
@Table(name="sg_user_black_list")
@IdClass(ChoiceUserBlackListId.class)
public class ChoiceUserBlackList implements Serializable{

	private static final long serialVersionUID = 8424720221026573604L;

	@Id
	@ManyToOne(fetch = FetchType.LAZY)
	private BlackListUser blackList;

	@Id
	@ManyToOne(fetch = FetchType.LAZY)
	private User user;

	@Column
	private ChoiceYesNoEnum choice;

	public ChoiceUserBlackList() {
	}


	public ChoiceUserBlackList(final BlackListUser blackList, final User user, final ChoiceYesNoEnum choice) {
		this.blackList = blackList;
		this.user = user;
		this.choice = choice;
	}

	public BlackListUser getBlackList() {
		return blackList;
	}

	public User getUser() {
		return user;
	}

	public ChoiceYesNoEnum getChoice() {
		return choice;
	}

	 public static class ChoiceUserBlackListId implements Serializable {

		private static final long serialVersionUID = 2095626328997022689L;

		private Long blackList;
        private Long user;

        public ChoiceUserBlackListId() {}

        public ChoiceUserBlackListId(final Long blackList, final Long user) {
            this.blackList = blackList;
            this.user = user;
        }

		@Override
		public int hashCode() {
			final int prime = 31;
			int result = 1;
			result = prime * result + ((blackList == null) ? 0 : blackList.hashCode());
			result = prime * result + ((user == null) ? 0 : user.hashCode());
			return result;
		}

		@Override
		public boolean equals(final Object obj) {
			if (this == obj) {
				return true;
			}
			if (obj == null) {
				return false;
			}
			if (getClass() != obj.getClass()) {
				return false;
			}
			final ChoiceUserBlackListId other = (ChoiceUserBlackListId) obj;
			if (blackList == null) {
				if (other.blackList != null) {
					return false;
				}
			} else if (!blackList.equals(other.blackList)) {
				return false;
			}
			if (user == null) {
				if (other.user != null) {
					return false;
				}
			} else if (!user.equals(other.user)) {
				return false;
			}
			return true;
		}

   }

}
