package com.socialgroup.server.entity;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OrderBy;
import javax.persistence.Table;

import com.socialgroup.server.endpoint.dto.AvailabilityDto;
import com.socialgroup.server.endpoint.dto.EventDto;
import com.socialgroup.server.endpoint.dto.FeedDto;
import com.socialgroup.server.endpoint.dto.PostDto;
import com.socialgroup.server.endpoint.dto.UserDto;
import com.socialgroup.server.entity.literals.UserRole;

@Entity
@Table(name = "sg_users")
public class User extends AbstractDate implements Serializable{

	private static final long serialVersionUID = -4755320225073186656L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(updatable = false, nullable = false)
	private Long id;

	@Column(name = "email")
	private String email;

	@Column(name = "avatar")
	private String avatar;

	@Column(name = "firstname")
	private String firstname;

	@Column(name = "lastname")
	private String lastname;

	@Column(name = "username")
	private String username;

	@Column(name = "password")
	private String password;

	@OrderBy("lastConnextion DESC")
	@OneToMany(mappedBy = "user", fetch=FetchType.EAGER, cascade=CascadeType.ALL)
	private Set<UserGroup> userGroups = new HashSet<>();

	@OneToMany
	@JoinColumn(name = "APP_USER_ID", referencedColumnName = "ID")
	private Set<UserRole> roles = new HashSet<>();

	@OneToMany(mappedBy = "user")
	private Set<Event> events = new HashSet<>();

	@OneToMany(mappedBy = "user")
	private Set<Feed> feeds = new HashSet<>();

	@ManyToOne(cascade = CascadeType.MERGE)
	@JoinColumn(name = "group_default_id", nullable = false)
	private Group group;

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getFirstname() {
		return firstname;
	}

	public void setFirstname(String firstname) {
		this.firstname = firstname;
	}

	public String getLastname() {
		return lastname;
	}

	public void setLastname(String lastname) {
		this.lastname = lastname;
	}

	public String getAvatar() {
		return avatar;
	}

	public void setAvatar(String avatar) {
		this.avatar = avatar;
	}

	public User() {
		super();
	}

	public User(Long id, String username, String password, Set<UserRole> roles) {
		super();
		this.id = id;
		this.username = username;
		this.password = password;
		this.roles = roles;
	}

	public User(Long id, String email, String avatar, String firstname, String lastname, String username,
			String password, Set<UserGroup> userGroups, Set<UserRole> roles, Set<Event> events, Set<Feed> feeds,
			Group group) {
		super();
		this.id = id;
		this.email = email;
		this.avatar = avatar;
		this.firstname = firstname;
		this.lastname = lastname;
		this.username = username;
		this.password = password;
		this.userGroups = userGroups;
		this.roles = roles;
		this.events = events;
		this.feeds = feeds;
		this.group = group;
	}

	public User(UserDto user) {
		super();
		this.id = user.getId();
		this.email = user.getEmail();
		this.firstname = user.getFirstname();
		this.lastname = user.getLastname();
		this.username = user.getUsername();
		this.password = user.getPassword();
		this.roles = user.getRoles();
		/*for (GroupDto g : user.getGroups()) {
			this.userGroups.add(new userGroup(g));
		}*/
		for (EventDto e : user.getEvents()) {
			if(e instanceof AvailabilityDto) {
				this.events.add(new Availability(e));
			}else {
				this.events.add(new Request(e));
			}
		}
		for (FeedDto filDto : user.getFeeds()) {
			if(filDto instanceof PostDto) {
				this.feeds.add(new Post(filDto));
			}else {
				this.feeds.add(new Comment(filDto));
			}
		}
		this.group = new Group(user.getGroup());
	}

	public User(User user) {
		super();
		this.id = user.getId();
		this.email = user.getEmail();
		this.firstname = user.getFirstname();
		this.lastname = user.getLastname();
		this.username = user.getUsername();
		this.password = user.getPassword();
		this.roles = user.getRoles();
		this.userGroups = user.getUserGroups();
		this.events = user.getEvents();
		this.feeds = user.getFeeds();
		this.group = user.getGroup();
	}

	public Long getId() {
		return id;
	}

	public String getUsername() {
		return username;
	}

	public String getPassword() {
		return password;
	}

	public Set<UserRole> getRoles() {
		return roles;
	}

	public Set<UserGroup> getUserGroups() {
		return userGroups;
	}

	public void setUserGroups(Set<UserGroup> userGroups) {
		this.userGroups = userGroups;
	}
	
	public void addUserGroup(UserGroup userGroup) {
		if(!this.userGroups.contains(userGroup)) {
			this.userGroups.add(userGroup);
		}
	}

	public void setId(Long id) {
		this.id = id;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public void setRoles(Set<UserRole> roles) {
		this.roles = roles;
	}


	public Set<Event> getEvents() {
		return events;
	}

	public void setEvents(Set<Event> events) {
		this.events = events;
	}

	public Group getGroup() {
		return group;
	}

	public void setGroup(Group group) {
		this.group = group;
	}

	public Set<Feed> getFeeds() {
		return feeds;
	}

	public void setFeeds(Set<Feed> feeds) {
		this.feeds = feeds;
	}

}
