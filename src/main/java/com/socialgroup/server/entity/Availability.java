package com.socialgroup.server.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Table;

import com.socialgroup.server.endpoint.dto.AvailabilityDto;
import com.socialgroup.server.endpoint.dto.EventDto;
import com.socialgroup.server.entity.literals.AvailabilityType;

@Entity
@Table(name="sg_availabilities")
public class Availability extends Event{
	
	private static final long serialVersionUID = -930437392459901442L;
	
	@Column(name = "availability_type", nullable = false, length = 25)
    @Enumerated(EnumType.STRING)
    private AvailabilityType availabilityType;
    
	public Availability() {
		super();
	}
	
	public Availability(EventDto eventDto) {
		super(eventDto);
	}
	
	public Availability(Event event) {
		super(event);
	}
	
	public Availability(AvailabilityDto availabilityDto) {
		super(availabilityDto);
		//this.availabilityType = availabilityDto.getType();
	}

	public Availability(AvailabilityType availabilityType) {
		super();
		this.availabilityType = availabilityType;
	}

	public AvailabilityType getAvailabilityType() {
		return availabilityType;
	}

	public void setAvailabilityType(AvailabilityType availabilityType) {
		this.availabilityType = availabilityType;
	}

}
