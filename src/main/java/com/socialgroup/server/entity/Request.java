package com.socialgroup.server.entity;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.socialgroup.server.endpoint.dto.EventDto;
import com.socialgroup.server.endpoint.dto.RequestDto;

@Entity
@Table(name="sg_requests")
public class Request extends Event{

	private static final long serialVersionUID = -8127855938772616496L;

	@Column(name="description", nullable=false)
    private String description;

	@Column(name="survey", nullable=false)
	private boolean survey = false;

	@OneToMany(cascade = CascadeType.MERGE, fetch=FetchType.LAZY)
	@JoinColumn(name = "request_id")
	private Set<RequestType> requestTypes = new HashSet<>();

	@OneToMany(mappedBy = "request", fetch=FetchType.LAZY, cascade = CascadeType.ALL)
	private Set<CommentSurvey> commentSurvey = new HashSet<>();

	public Request() {
		super();
	}

	public Request(final RequestDto requestDto) {
		super();
		this.survey = requestDto.isSurvey();
		this.description = requestDto.getDescription();
	}

	public Request(final String description, final boolean survey, final Set<RequestType> requestTypes) {
		super();
		this.description = description;
		this.survey = survey;
		this.requestTypes = requestTypes;
	}

	public Request(final EventDto e) {
		super(e);
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(final String description) {
		this.description = description;
	}

	public Set<RequestType> getRequestTypes() {
		return requestTypes;
	}

	public void setRequestTypes(final Set<RequestType> requestTypes) {
		this.requestTypes = requestTypes;
	}

	public void removeAllRequestsType() {
		final Collection<RequestType> requestTypes = new ArrayList<>(this.requestTypes);
		for (final RequestType requestType : requestTypes) {
			this.removeRequestType(requestType);
		}
	}

	public void removeRequestType(final RequestType requestType) {
		if(this.requestTypes.contains(requestType)){
			this.requestTypes.remove(requestType);
		}
	}

	public boolean isSurvey() {
		return survey;
	}

	public void setSurvey(final boolean survey) {
		this.survey = survey;
	}

	public void addRequestType(final RequestType requestType) {
		this.requestTypes.add(requestType);
	}

	public void addCommentSurvey(final CommentSurvey commentSurvey) {
		this.commentSurvey.add(commentSurvey);
	}

	public void removeCommentSurvey(final CommentSurvey commentSurvey) {
		if(this.commentSurvey.contains(commentSurvey)){
			this.commentSurvey.remove(commentSurvey);
		}
	}

	public Set<CommentSurvey> getCommentSurvey() {
		return commentSurvey;
	}

	public void setCommentSurvey(final Set<CommentSurvey> commentSurvey) {
		this.commentSurvey = commentSurvey;
	}

}
