package com.socialgroup.server.entity;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OrderBy;
import javax.persistence.Table;

import com.socialgroup.server.endpoint.dto.FeedDto;

@Entity
@Table(name = "sg_posts")
public class Post extends Feed {

	private static final long serialVersionUID = 7635373353157710563L;

	@OrderBy("created ASC")
	@OneToMany(mappedBy = "post", cascade = CascadeType.REMOVE)
	private Set<Comment> comments = new HashSet<>();

	@ManyToOne()
	@JoinColumn(name = "group_id")
	private Group group;

	public Post() {
		super();
	}

	public Post(final Set<Comment> comments, final Group group) {
		super();
		this.comments = comments;
		this.group = group;
	}

	public Post(final FeedDto feedDto) {
		super(feedDto);
	}

	public Group getGroup() {
		return group;
	}

	public void setGroup(final Group group) {
		this.group = group;
	}

	public Set<Comment> getComments() {
		return comments;
	}

	public void setComments(final Set<Comment> comments) {
		this.comments = comments;
	}

}
