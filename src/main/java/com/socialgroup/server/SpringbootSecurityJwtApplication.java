package com.socialgroup.server;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.scheduling.annotation.EnableScheduling;

import io.sniffy.boot.EnableSniffy;

/**
 * Sample application for demonstrating security with JWT Tokens
 *
 * @author vladimir.stankovic
 *
 * Aug 3, 2016
 */
@SpringBootApplication
@EnableConfigurationProperties
@EnableAutoConfiguration
@EnableSniffy
@EnableScheduling
public class SpringbootSecurityJwtApplication {

//	@Override
//    protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
//        return application.sources(SpringbootSecurityJwtApplication.class);
//    }

	public static void main(final String[] args) {
		SpringApplication.run(SpringbootSecurityJwtApplication.class, args);
	}
}
