/*
INSERT INTO `sg_groups`(`id`, `name`) VALUES (1, 'Groupe de test');
INSERT INTO `sg_users` (`id`, `created`, `updated`, `avatar`, `email`, `firstname`, `lastname`, `password`, `username`, `group_default_id`) VALUES
(1, '2019-06-09 19:24:27', '2019-06-09 19:24:27', '', 'charlybeaugrand@gmail.com', 'Charly', 'Beaugrand', '$2a$10$bnC26zz//2cavYoSCrlHdecWF8tkGfPodlHcYwlACBBwJvcEf0p2G', 'charly', 1),
(8, '2019-06-11 23:22:23', '2019-06-11 23:22:23', '', 'test@test.fr', 'test', 'TEST', '$2a$10$ZAZdOU8aHdd6mRKyUjVRLu7bIUT2uCe5vB8kKwiODRFWf/J/AS4Yu', 'test', 1);

INSERT INTO `sg_user_groups` (`user_id`, `group_id`, `connected`) VALUES
(1, 1, '2018-04-10 01:02:32'),
(8, 1, '2018-04-10 01:02:32');

INSERT INTO `sg_user_roles` (`role`, `app_user_id`) VALUES
('MEMBER', 1),
('MEMBER', 8);

INSERT INTO `sg_feeds`(`id`, `created`, `updated`,`message`, `user_id`) VALUES ( 1, '2018-04-10 01:01:01', null, 'Quand est-ce que l\'on se refait une soirée ???', 1);
INSERT INTO `sg_posts`(`id`, `group_id`) VALUES ( 1, 1);
INSERT INTO `sg_feeds`(`id`, `created`, `updated`, `message`, `user_id`) VALUES ( 2, '2018-04-10 01:02:32', null, 'Quand tu seras dans le coin, mouahhhh :-)', 1);
INSERT INTO `sg_feeds`(`id`, `created`, `updated`, `message`, `user_id`) VALUES ( 3, '2018-04-10 01:02:32', '2018-04-10 03:02:32', 'Ou pas !!!', 1);
INSERT INTO `sg_comments`(`id`,`master_comment_id`, `post_id`, `sub_comment`) VALUES ( 2, null, 1, b'0');
INSERT INTO `sg_comments`(`id`,`master_comment_id`, `post_id`, `sub_comment`) VALUES ( 3, 2, 1, b'0');



INSERT INTO `sg_events`(`id`, `created`, `updated`, `end_time`, `start_time`, `group_id`, `post_id`, `user_id`) VALUES (1, '2018-04-10 01:02:32', null, '2018-04-08 00:00:00', '2018-04-18 00:00:00', 1, 1, 1);
INSERT INTO `sg_events`(`id`, `created`, `updated`, `end_time`, `start_time`, `group_id`, `user_id`) VALUES (2, '2018-04-10 01:02:32', '2018-04-10 03:02:32', '2018-04-18 00:00:00', '2018-04-28 00:00:00', 1, 1);
INSERT INTO `sg_events`(`id`, `created`, `updated`, `end_time`, `start_time`, `group_id`, `user_id`) VALUES (3, '2018-04-10 01:02:32', '2018-04-10 03:02:32', '2018-04-18 00:00:00', '2018-04-28 00:00:00', 1, 1);
INSERT INTO `sg_events`(`id`, `created`, `updated`, `end_time`, `start_time`, `group_id`, `user_id`) VALUES (4, '2018-04-10 01:02:32', '2018-04-10 03:02:32', '2018-04-18 00:00:00', '2018-04-28 00:00:00', 1, 1);

INSERT INTO `sg_availabilities`(`availability_type`, `id`) VALUES ('A', 4);



INSERT INTO `sg_requests`(`description`, `id`, `survey`) VALUES ( 'On va faire un Bowling ?', 1, 0);
INSERT INTO `sg_request_types`(`id`, `name`, `request_id`) VALUES (4, 'Bowling', 1);
--INSERT INTO `sg_request_request_types`(`request_id`, `request_type_id`) VALUES (1, 4);

INSERT INTO `sg_requests`(`description`, `id`, `survey`) VALUES ( 'On va voir Avengers 3 ?', 2, 0);
INSERT INTO `sg_request_types`(`id`, `name`, request_id) VALUES (1, 'Cinéma', 2);

--INSERT INTO `sg_request_request_types`(`request_id`, `request_type_id`) VALUES (2, 1);
*/

/*
-- A décommenter lors de la mise en place du système de sondage

INSERT INTO `sg_request_types`(`id`, `name`) VALUES (2, '1 personnes');
INSERT INTO `sg_request_types`(`id`, `name`) VALUES (3, '2 personnes');
INSERT INTO `sg_requests`(`description`, `id`, `survey`) VALUES ( 'Combien sont Guay :-) ?', 3, 1);
INSERT INTO `sg_request_request_types`(`request_id`, `request_type_id`) VALUES (3, 2);
INSERT INTO `sg_request_request_types`(`request_id`, `request_type_id`) VALUES (3, 3);


INSERT INTO `sg_comments_survey`(`id`, `created`, `request_type_id`, `request_id`) VALUES (1, '2018-04-10 01:02:32', 2, 3);

*/